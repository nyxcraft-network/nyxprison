package net.nyxcraft.dev.prison.runnables;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.prison.NyxPrison;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class GuardRunnable implements Runnable {

    private int counts = 5;
    public Player target;

    public GuardRunnable(Player target) {
        this.target = target;
    }

    @Override
    public void run() {
        MessageFormatter.sendGeneralMessage(target, "" + counts);
        target.sendMessage(ChatColor.GOLD + "" + counts);
        counts--;
        if (counts != 0) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), this, 20);
        }
    }

}

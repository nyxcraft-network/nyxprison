package net.nyxcraft.dev.prison.world;

import com.sk89q.jnbt.*;
import com.sk89q.worldedit.blocks.BaseBlock;

import net.nyxcraft.dev.nyxutils.reflection.CommonReflection;
import net.nyxcraft.dev.nyxutils.reflection.VersionHandler;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;

public class WorldEditUtils {

    /*
     * A megaton of reflection!
     */
    private static Class<?> craftWorld;
    private static Class<?> blockPosition;
    private static Class<?> tileEntity;
    private static Class<?> nbtTagCompound;
    private static Class<?> nbtBase;
    private static Class<?> nbtTagByte;
    private static Class<?> nbtTagByteArray;
    private static Class<?> nbtTagShort;
    private static Class<?> nbtTagInt;
    private static Class<?> nbtTagIntArray;
    private static Class<?> nbtTagLong;
    private static Class<?> nbtTagFloat;
    private static Class<?> nbtTagDouble;
    private static Class<?> nbtTagString;
    private static Class<?> nbtTagList;
    private static Constructor<?> blockPositionConstructor;
    private static Constructor<?> nbtTagByteConstructor;
    private static Constructor<?> nbtTagByteArrayConstructor;
    private static Constructor<?> nbtTagShortConstructor;
    private static Constructor<?> nbtTagIntConstructor;
    private static Constructor<?> nbtTagIntArrayConstructor;
    private static Constructor<?> nbtTagLongConstructor;
    private static Constructor<?> nbtTagFloatConstructor;
    private static Constructor<?> nbtTagDoubleConstructor;
    private static Constructor<?> nbtTagStringConstructor;
    private static Method getTileEntity;
    private static Method set;
    private static Method nbtCreateTagMethod;
    private static Method add;
    private static Method a;

    static {
        try {
            craftWorld = VersionHandler.getOBCClass("CraftWorld");
            blockPosition = VersionHandler.getNMSClass("BlockPosition");
            tileEntity = VersionHandler.getNMSClass("TileEntity");
            nbtTagCompound = VersionHandler.getNMSClass("NBTTagCompound");
            nbtBase = VersionHandler.getNMSClass("NBTBase");
            nbtTagByte = VersionHandler.getNMSClass("NBTTagByte");
            nbtTagByteArray = VersionHandler.getNMSClass("NBTTagByteArray");
            nbtTagShort = VersionHandler.getNMSClass("NBTTagShort");
            nbtTagInt = VersionHandler.getNMSClass("NBTTagInt");
            nbtTagIntArray = VersionHandler.getNMSClass("NBTTagIntArray");
            nbtTagLong = VersionHandler.getNMSClass("NBTTagLong");
            nbtTagFloat = VersionHandler.getNMSClass("NBTTagFloat");
            nbtTagDouble = VersionHandler.getNMSClass("NBTTagDouble");
            nbtTagString = VersionHandler.getNMSClass("NBTTagString");
            nbtTagList = VersionHandler.getNMSClass("NBTTagList");
            blockPositionConstructor = blockPosition.getConstructor(new Class<?>[]{ int.class, int.class, int.class });
            getTileEntity = CommonReflection.getMethod(craftWorld, "getTileEntity", new Class<?>[]{ blockPosition });
            set = CommonReflection.getMethod(nbtTagCompound, "set", new Class<?>[]{ String.class, nbtBase });

            nbtCreateTagMethod = nbtBase.getDeclaredMethod("createTag", new Class<?>[]{ byte.class });
            nbtCreateTagMethod.setAccessible(true);

            nbtTagByteConstructor = nbtTagByte.getConstructor(new Class<?>[]{ byte.class });
            nbtTagByteArrayConstructor = nbtTagByteArray.getConstructor(new Class<?>[]{ byte[].class });
            nbtTagShortConstructor = nbtTagShort.getConstructor(new Class<?>[]{ short.class });
            nbtTagIntConstructor = nbtTagInt.getConstructor(new Class<?>[]{ int.class });
            nbtTagIntArrayConstructor = nbtTagIntArray.getConstructor(new Class<?>[]{ int[].class });
            nbtTagLongConstructor = nbtTagLong.getConstructor(new Class<?>[]{ long.class });
            nbtTagFloatConstructor = nbtTagFloat.getConstructor(new Class<?>[]{ float.class });
            nbtTagDoubleConstructor = nbtTagDouble.getConstructor(new Class<?>[]{ double.class });
            nbtTagStringConstructor = nbtTagString.getConstructor(new Class<?>[]{ String.class });
            add = CommonReflection.getMethod(nbtTagList, "add", new Class<?>[] { nbtBase });
            a = CommonReflection.getMethod(tileEntity, "a", new Class<?>[] { nbtTagCompound });
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static WorldEditPlugin getWorldEdit() {
        return (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
    }

    public static CuboidSelection getSelection(Player player) {
        Selection selection = getWorldEdit().getSelection(player);
        if (!(selection instanceof CuboidSelection)) {
            return null;
        } else {
            return (CuboidSelection) selection;
        }

    }

    @SuppressWarnings("deprecation")
    public static boolean setBlock(Location location, BaseBlock block, boolean notifyAndLight) {
        Object craftWorld = CommonReflection.getHandle(location.getWorld());
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        boolean changed = location.getBlock().setTypeIdAndData(block.getId(), (byte)block.getData(), notifyAndLight);

        CompoundTag nativeTag = block.getNbtData();
        if (nativeTag != null) {
            Object tileEntity = null;
            try {
                tileEntity = getTileEntity.invoke(craftWorld, blockPositionConstructor.newInstance(x, y, z));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }

            if (tileEntity != null) {
                Object tag = nbtTagCompound.cast(fromNative(nativeTag));
                try {
                    set.invoke("x", nbtTagIntConstructor.newInstance(x));
                    set.invoke("y", nbtTagIntConstructor.newInstance(y));
                    set.invoke("z", nbtTagIntConstructor.newInstance(z));
                    readTagIntoTileEntity(tag, tileEntity);
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        return changed;
    }

    private static void readTagIntoTileEntity(Object tag, Object tileEntity) throws InstantiationException, IllegalAccessException, InvocationTargetException
    {
        a.invoke(tileEntity, tag);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static Object fromNative(Tag foreign) {
        if (foreign == null) {
            return null;
        }

        Map.Entry<String, Tag> entry;
        if ((foreign instanceof CompoundTag)) {
            Object tag = null;
            try {
                tag = nbtTagCompound.newInstance();

                for (Iterator localIterator = ((CompoundTag)foreign).getValue().entrySet().iterator(); localIterator.hasNext();) {
                    entry = (Map.Entry)localIterator.next();
                    set.invoke(tag, (String)entry.getKey(), fromNative((Tag)entry.getValue()));
                }
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }

            return tag;
        }
        try {
            if ((foreign instanceof ByteTag)) {
                return nbtTagByteConstructor.newInstance(((ByteTag)foreign).getValue().byteValue());
            }
            if ((foreign instanceof ByteArrayTag)) {
                return nbtTagByteArrayConstructor.newInstance(((ByteArrayTag) foreign).getValue());
            }
            if ((foreign instanceof DoubleTag)) {
                return nbtTagDoubleConstructor.newInstance(((DoubleTag) foreign).getValue().doubleValue());
            }
            if ((foreign instanceof FloatTag)) {
                return nbtTagFloatConstructor.newInstance(((FloatTag) foreign).getValue().floatValue());
            }
            if ((foreign instanceof IntTag)) {
                return nbtTagIntConstructor.newInstance(((IntTag) foreign).getValue().intValue());
            }
            if ((foreign instanceof IntArrayTag)) {
                return nbtTagIntArrayConstructor.newInstance(((IntArrayTag) foreign).getValue());
            }
            if ((foreign instanceof ListTag))
            {
                Object tag = nbtTagList.newInstance();
                ListTag foreignList = (ListTag)foreign;
                for (Tag t : foreignList.getValue()) {
                    add.invoke(tag, fromNative(t));
                }
                return tag;
            }
            if ((foreign instanceof LongTag)) {
                return nbtTagLongConstructor.newInstance(((LongTag) foreign).getValue().longValue());
            }
            if ((foreign instanceof ShortTag)) {
                return nbtTagShortConstructor.newInstance(((ShortTag)foreign).getValue().shortValue());
            }
            if ((foreign instanceof StringTag)) {
                return nbtTagStringConstructor.newInstance(((StringTag) foreign).getValue());
            }
            if ((foreign instanceof EndTag)) {
                try
                {
                    return nbtBase.cast(nbtCreateTagMethod.invoke(null, new Object[] { Byte.valueOf((byte) 0) }));
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        throw new IllegalArgumentException("Don't know how to make NMS " + foreign.getClass().getCanonicalName());
    }
}

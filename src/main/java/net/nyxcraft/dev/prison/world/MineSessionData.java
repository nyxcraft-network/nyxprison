package net.nyxcraft.dev.prison.world;

import net.nyxcraft.dev.prison.database.entities.Mine;

public class MineSessionData {
    private Mine mine;
    private double brokenBlocks;
    private double normalSize;

    public MineSessionData(Mine mine, double brokenBlocks, double normalSize) {
        this.mine = mine;
        this.brokenBlocks = brokenBlocks;
        this.normalSize = normalSize;
    }

    public void addBlock() {
        if (brokenBlocks++ / normalSize >= .15) {
            brokenBlocks = 0;
            mine.reset();
        }
    }

    public Mine getMine() {
        return mine;
    }

    public double getBrokenBlocks() {
        return brokenBlocks;
    }

    public double getNormalSize() {
        return normalSize;
    }
}

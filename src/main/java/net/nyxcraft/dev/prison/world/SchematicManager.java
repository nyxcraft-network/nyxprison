package net.nyxcraft.dev.prison.world;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.schematic.SchematicFormat;
import com.sk89q.worldedit.world.DataException;

import net.nyxcraft.dev.prison.NyxPrison;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("deprecation")
public class SchematicManager {

    private CuboidClipboard hallSchematic;
    private CuboidClipboard cellSchematic;

    public SchematicManager() {
        reload();
    }

    public CuboidClipboard getHallSchematic() {
        return hallSchematic;
    }

    public CuboidClipboard getCellSchematic() {
        return cellSchematic;
    }

    public CuboidClipboard loadSchematic(String name) {
        try {
            return SchematicFormat.MCEDIT.load(new File(NyxPrison.getInstance().getDataFolder(), name));
        } catch (DataException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void reload() {
        hallSchematic = loadSchematic("NCHall.schematic");
        cellSchematic = loadSchematic("NCCell.schematic");
    }

    public boolean validate() {
        if (hallSchematic == null || cellSchematic == null) {
            reload();
        }

        return (hallSchematic != null && cellSchematic != null);
    }
}

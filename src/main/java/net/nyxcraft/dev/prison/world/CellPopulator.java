package net.nyxcraft.dev.prison.world;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.nyxcraft.dev.prison.NyxPrison;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.generator.BlockPopulator;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;

@SuppressWarnings("deprecation")
public class CellPopulator extends BlockPopulator {

    private SchematicManager schematicManager;

    public CellPopulator() {
        schematicManager = NyxPrison.getInstance().getSchematicManager();
    }

    @Override
    public void populate(World world, Random random, Chunk chunk) {
        if (schematicManager == null) {
            NyxPrison.getInstance().reloadConfig();
        }

        if (!schematicManager.validate()) {
            NyxPrison.getInstance().getLogger().info("Populator schematics could not be validated!");
            return;
        }

        Map<Location, BaseBlock> torches = new HashMap<>();
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                for (int z = 0; z < 16; z++) {
                    CuboidClipboard schematic = chunk.getZ() % 2 == 0 ? schematicManager.getCellSchematic() : schematicManager.getHallSchematic();
                    Block block = chunk.getBlock(x, y + 51, z);

                    if (schematic.getBlock(new Vector(x, y, z)).getId() != 0) {
                        BaseBlock base = schematic.getBlock(new Vector(x, y, z));
                        Material material = Material.getMaterial(base.getId());

                        if (material == Material.TORCH) {
                            torches.put(block.getLocation(), base);
                            continue;
                        }

                        block.setType(material);
                        block.setData((byte) base.getData());
                    }
                }
            }
        }

        for (Map.Entry<Location, BaseBlock> entry : torches.entrySet()) {
            WorldEditUtils.setBlock(entry.getKey(), entry.getValue(), true);
        }

        torches.clear();
    }
}

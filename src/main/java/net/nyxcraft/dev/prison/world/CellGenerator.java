package net.nyxcraft.dev.prison.world;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.*;

public class CellGenerator extends ChunkGenerator {

    private short[] layer;

    @SuppressWarnings("deprecation")
    public CellGenerator() {
        layer = new short[50];
        Arrays.fill(layer, (short) Material.STONE.getId());
        layer[0] = (short) Material.BEDROCK.getId();
    }

    @Override
    public short[][] generateExtBlockSections(World world, Random random, int x, int z, BiomeGrid biomes)
    {
        int maxHeight = world.getMaxHeight();
        short[][] result = new short[maxHeight / 16][]; // 16x16x16 chunks
        for (int i = 0; i < layer.length; i += 16)
        {
            result[i >> 4] = new short[4096];
            for (int y = 0; y < Math.min(16, layer.length - i); y++)
            {
                Arrays.fill(result[i >> 4], y * 16 * 16, (y + 1) * 16 * 16, layer[i + y]);
            }
        }

        return result;
    }

    @Override
    public Location getFixedSpawnLocation(World world, Random random) {
        return new Location(world, 0, 52, 0);
    }

    /**
     * Returns a list of all of the block populators (that do "little" features)
     * to be called after the chunk generator
     */
    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        return Arrays.asList(new CellPopulator());
    }
}
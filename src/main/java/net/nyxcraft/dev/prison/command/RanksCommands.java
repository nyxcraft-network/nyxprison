package net.nyxcraft.dev.prison.command;

import java.text.DecimalFormat;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.api.PrisonerDataAPI;
import net.nyxcraft.dev.prison.listener.InteractListener;
import net.nyxcraft.dev.prison.player.PrisonRank;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

public class RanksCommands {

    public RanksCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "rankup", RanksCommands::rankup);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "prestige", RanksCommands::prestige);
    }

    public static void rankup(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        PrisonRank nextRank = PrisonRank.nextRank(prisoner.rank);

        if (nextRank == null) {
            MessageFormatter.sendErrorMessage(player, "You are at the highest available rank.");
            return;
        }

        if (nextRank.price > prisoner.balance) {
            MessageFormatter.sendErrorMessage(player, "You need another $" + formatNumber((nextRank.price - prisoner.balance)) + " to rank up");
            return;
        }

        prisoner.balance -= nextRank.price;
        prisoner.rank = PrisonRank.values()[prisoner.rank.ordinal() + 1];
        PrisonerDataAPI.rankUp(prisoner);
        prisoner.updateScoreboard();

        Bukkit.broadcastMessage(ChatColor.GOLD + player.getName() + ChatColor.GREEN + " has ranked up to " + ChatColor.GOLD + prisoner.rank.name + "!");
    }

    public static void prestige(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        PrisonRank nextRank = PrisonRank.nextRank(prisoner.rank);

        if (nextRank == null) {
            if (InteractListener.getSelling().contains(player.getUniqueId())) {
                MessageFormatter.sendErrorMessage(player, "You cannot prestige while selling items.");
                return;
            }

            prisoner.rank = PrisonRank.A;
            prisoner.prestige += 1;
            prisoner.balance = 0;
            prisoner.pickData.level = 0;
            prisoner.pickData.exp = 0;
            prisoner.pickData.upgrades = 0;
            prisoner.pickData.enchantments.clear();
            PrisonerDataAPI.prestige(prisoner);
            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }
            Bukkit.broadcastMessage(ChatColor.GOLD + player.getName() + ChatColor.GREEN + " has reached prestige level " + ChatColor.GOLD + prisoner.prestige + "!");
            prisoner.update();
            return;
        }

        MessageFormatter.sendErrorMessage(player, "You must reach rank Z to prestige.");
    }

    public static String formatNumber(double num) {
        DecimalFormat format = new DecimalFormat("0.###");
        if (num > 1000000000000000000000000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000000000000000000000000.0) + " cent";
        }
        if (num > 1000000000000000000000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000000000000000000000.0) + " vigint";
        }
        if (num > 1000000000000000000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000000000000000000.0) + " novemdec";
        }
        if (num > 1000000000000000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000000000000000.0) + " octodec";
        }
        if (num > 1000000000000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000000000000.0) + " septendec";
        }
        if (num > 1000000000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000000000.0) + " sexdec";
        }
        if (num > 1000000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000000.0) + " quindec";
        }
        if (num > 1000000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000000.0) + " quattuordec";
        }
        if (num > 1000000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000000.0) + " tredec";
        }
        if (num > 1000000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000000.0) + " duodec";
        }
        if (num > 1000000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000000.0) + " undec";
        }
        if (num > 1000000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000000.0) + " dec";
        }
        if (num > 1000000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000000.0) + " non";
        }
        if (num > 1000000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000000.0) + " oct";
        }
        if (num > 1000000000000000000000000.0) {
            return format.format(num / 1000000000000000000000000.0) + " sept";
        }
        if (num > 1000000000000000000000.0) {
            return format.format(num / 1000000000000000000000.0) + " sext";
        }
        if (num > 1000000000000000000.0) {
            return format.format(num / 1000000000000000000.0) + " quint";
        }
        if (num > 1000000000000000.0) {
            return format.format(num / 1000000000000000.0) + " quadr";
        }
        if (num > 1000000000000.0) {
            return format.format(num / 1000000000000.0) + " tril";
        }
        if (num > 1000000000.0) {
            return format.format(num / 1000000000.0) + " bil";
        }
        if (num > 1000000.0) {
            return format.format(num / 1000000.0) + " mil";
        }
        if (num > 1000.0) {
            return format.format(num / 1000.0) + "k";
        } else {
            return num + "";
        }
    }

}

package net.nyxcraft.dev.prison.command;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;

import org.bukkit.entity.Player;

public class DonorCommands {

    public DonorCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "craft", Rank.HERO, DonorCommands::craftCommand);
    }

    public static void craftCommand(Player player, String[] args) {
        MessageFormatter.sendSuccessMessage(player, "Opened craft window.");
        player.openWorkbench(null, true);
    }

}

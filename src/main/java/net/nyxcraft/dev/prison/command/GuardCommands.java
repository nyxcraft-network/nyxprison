package net.nyxcraft.dev.prison.command;

import java.util.HashMap;
import java.util.UUID;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.api.PrisonerDataAPI;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;
import net.nyxcraft.dev.prison.runnables.GuardRunnable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class GuardCommands {

    private static HashMap<String, Integer> requests = new HashMap<String, Integer>();

    public GuardCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "guard", GuardCommands::guardCommand);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "toggleguard", GuardCommands::toggleGuard);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "addguard", Rank.ADMINISTRATOR, GuardCommands::addGuard);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "removeguard", Rank.ADMINISTRATOR, GuardCommands::removeGuard);
    }

    public static void guardCommand(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (!prisoner.guard) {
            MessageFormatter.sendErrorMessage(player, "You need to be a guard to do that.");
            return;
        }
        if (!prisoner.guardEnabled) {
            MessageFormatter.sendErrorMessage(player, "You need to be in guard mode to do that.");
            return;
        }
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(player, "/guard [player] [contraband]");
            return;
        }
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            MessageFormatter.sendErrorMessage(player, "That player is not online.");
            return;
        }
        MessageFormatter.sendGeneralMessage(target, player.getName() + " has requested your " + args[1]);
        MessageFormatter.sendSuccessMessage(player, "You have requested " + target.getName() + "'s " + args[1]);
        requests.put(target.getName(), 5);
        Runnable runable = new GuardRunnable(target);
        Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), runable, 20);

    }

    public static void toggleGuard(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (!prisoner.guard) {
            MessageFormatter.sendErrorMessage(player, "You need to be a guard to do that.");
            return;
        }

        prisoner.guardEnabled = !prisoner.guardEnabled;
        prisoner.update();
        String status = prisoner.guardEnabled ? "enabled" : "disabled";
        MessageFormatter.sendSuccessMessage(player, "You " + status + " guard mode");
    }

    public static void addGuard(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/addguard <player>");
        }
        Player target = Bukkit.getPlayer(args[0]);
        PrisonerSessionData prisoner;
        PrisonerData data;
        if (target == null) {
            data = PrisonDatabaseManager.getPrisonerData(args[0]);
            prisoner = null;
        } else {
            prisoner = NyxPrison.getInstance().getPrisonerSessionData(target.getUniqueId());
            data = null;
        }

        if (prisoner != null) {
            if (prisoner.guard) {
                MessageFormatter.sendErrorMessage(player, target.getName() + " is already a Guard");
                return;
            }
            prisoner.guard = true;
            PrisonerDataAPI.setGuard(prisoner.uuid, true, false);
            prisoner.update();
            MessageFormatter.sendInfoMessage(target, "You are now a Guard! Congratulations");
            MessageFormatter.sendSuccessMessage(player, target.getName() + " is now a Guard!");
            return;
        }

        if (data == null) {
            MessageFormatter.sendErrorMessage(player, args[0] + " does not exist. You sure you entered the name correctly?");
            return;
        }

        if (data.guard) {
            MessageFormatter.sendErrorMessage(player, args[0] + " is already a Guard");
            return;
        }

        data.guard = true;
        PrisonerDataAPI.setGuard(UUID.fromString(data.uuid), true, false);
        MessageFormatter.sendSuccessMessage(player, args[0] + " is now a Guard!");
    }

    public static void removeGuard(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/addguard <player>");
        }
        Player target = Bukkit.getPlayer(args[0]);
        PrisonerSessionData prisoner;
        PrisonerData data;
        if (target == null) {
            data = PrisonDatabaseManager.getPrisonerData(args[0]);
            prisoner = null;
        } else {
            prisoner = NyxPrison.getInstance().getPrisonerSessionData(target.getUniqueId());
            data = null;
        }

        if (prisoner != null) {
            if (!prisoner.guard) {
                MessageFormatter.sendErrorMessage(player, target.getName() + " is no Guard");
                return;
            }
            prisoner.guard = false;
            PrisonerDataAPI.setGuard(prisoner.uuid, false, false);
            prisoner.update();
            MessageFormatter.sendInfoMessage(target, "You are no longer a Guard!");
            MessageFormatter.sendSuccessMessage(player, target.getName() + " is no longer a Guard!");
            return;
        }

        if (data == null) {
            MessageFormatter.sendErrorMessage(player, args[0] + " does not exist. You sure you entered the name correctly?");
            return;
        }

        if (!data.guard) {
            MessageFormatter.sendErrorMessage(player, args[0] + " is no Guard");
            return;
        }

        data.guard = false;
        PrisonerDataAPI.setGuard(UUID.fromString(data.uuid), false, false);
        MessageFormatter.sendSuccessMessage(player, args[0] + " is no longer a Guard!");
    }
}

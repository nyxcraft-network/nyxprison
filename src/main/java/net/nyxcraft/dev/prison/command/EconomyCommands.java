package net.nyxcraft.dev.prison.command;

import java.util.UUID;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.api.PrisonerDataAPI;
import net.nyxcraft.dev.prison.database.dao.GuardDAO;
import net.nyxcraft.dev.prison.database.dao.MerchantDAO;
import net.nyxcraft.dev.prison.database.entities.Guard;
import net.nyxcraft.dev.prison.database.entities.Merchant;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;
import net.nyxcraft.dev.prison.economy.GuardEntity;
import net.nyxcraft.dev.prison.economy.MerchantEntity;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class EconomyCommands {

    public EconomyCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "balance", EconomyCommands::balance);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "bal", EconomyCommands::balance);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "money", EconomyCommands::balance);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "pay", EconomyCommands::pay);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "forcepay", Rank.ADMINISTRATOR, EconomyCommands::forcePay);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "stats", EconomyCommands::stats);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "spawnguard", Rank.ADMINISTRATOR, EconomyCommands::spawnGuard);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "spawnmerchant", Rank.ADMINISTRATOR, EconomyCommands::spawnMerchant);
    }

    @SuppressWarnings("deprecation")
    public static void balance(Player player, String[] args) {

        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());

        if (args.length == 0) {
            MessageFormatter.sendGeneralMessage(player, "Balance: " + ChatColor.WHITE + "$" + RanksCommands.formatNumber(prisoner.balance));
            return;
        } else if (args.length == 1) {
            PrisonerData target = PrisonDatabaseManager.getPrisonerData(Bukkit.getOfflinePlayer(args[0]).getUniqueId());
            if (target == null) {
                MessageFormatter.sendErrorMessage(player, "Player not found.");
                return;
            } else {
                MessageFormatter.sendGeneralMessage(player, Bukkit.getOfflinePlayer(UUID.fromString(target.uuid)).getName() + "'s balance: $" + ChatColor.WHITE + RanksCommands.formatNumber(target.balance));
                return;
            }
        } else {
            MessageFormatter.sendGeneralMessage(player, "Balance: " + ChatColor.WHITE + "$" + RanksCommands.formatNumber(prisoner.balance));
        }

    }

    public static void pay(Player player, String[] args) {

        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(player, "/pay [target] [amount]");
            return;
        }

        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        PrisonerData targetData = null;
        PrisonerSessionData target = null;

        if (Bukkit.getPlayer(args[0]) == null) {
            targetData = PrisonDatabaseManager.getPrisonerData(args[0]);
            if (targetData != null) {
                target = new PrisonerSessionData(UUID.fromString(targetData.uuid), targetData);
            }
        } else {
            target = NyxPrison.getInstance().getPrisonerSessionData(Bukkit.getPlayer(args[0]).getUniqueId());
        }

        if (target == null) {
            MessageFormatter.sendErrorMessage(player, "Player not found.");
            return;
        }

        double amount = 0;
        try {
            amount = Math.abs(Double.parseDouble(args[1]));
        } catch (NumberFormatException ex) {
            MessageFormatter.sendUsageMessage(player, "/pay [target] [amount]");
            return;
        }

        if (prisoner.balance < amount) {
            MessageFormatter.sendErrorMessage(player, "You don't have enough money.");
            return;
        }

        prisoner.balance -= amount;
        PrisonerDataAPI.setBalance(prisoner);
        target.balance += amount;
        PrisonerDataAPI.setBalance(target);

        prisoner.updateScoreboard();
        target.updateScoreboard();

        MessageFormatter.sendSuccessMessage(player, "You sent " + ChatColor.GREEN + "$" + RanksCommands.formatNumber(amount) + ChatColor.GRAY + " to " + args[0] + ".");
        if (Bukkit.getPlayer(target.uuid) != null) {
            MessageFormatter.sendGeneralMessage(Bukkit.getPlayer(target.uuid), "You received $" + ChatColor.GREEN + RanksCommands.formatNumber(amount) + ChatColor.GOLD + " from " + player.getName());
        }

    }

    public static void forcePay(Player player, String[] args) {

        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(player, "/forcepay [target] [amount]");
            return;
        }

        Player p = Bukkit.getPlayer(args[0]);
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(p.getUniqueId());
        if (prisoner == null) {
            MessageFormatter.sendErrorMessage(player, "Player not found.");
            return;
        }

        double amount = 0;
        try {
            amount = Math.abs(Double.parseDouble(args[1]));
        } catch (NumberFormatException ex) {
            MessageFormatter.sendUsageMessage(player, "/forcepay [target] [amount]");
            return;
        }

        prisoner.balance += amount;
        prisoner.updateScoreboard();

        MessageFormatter.sendSuccessMessage(player, "You have force sent " + ChatColor.GREEN + "$" + RanksCommands.formatNumber(amount) + ChatColor.GRAY + " to " + p.getName() + ".");
        MessageFormatter.sendGeneralMessage(p, "You received $" + ChatColor.GREEN + RanksCommands.formatNumber(amount) + ".");

    }

    @SuppressWarnings("unused")
    public static void spawnGuard(Player player, String[] args) {
        Location loc = player.getLocation();
        int newId = 0;
        for (Guard g : GuardDAO.getInstance().createQuery()) {
            newId++;
        }
        Guard guard = new Guard(newId + 1);
        guard.location = PrisonCommands.locationToString(loc);
        GuardDAO.getInstance().save(guard);
        GuardEntity.loadGuard(loc, guard.id);
        MessageFormatter.sendSuccessMessage(player, "Guard spawned and saved.");
    }

    @SuppressWarnings("unused")
    public static void spawnMerchant(Player player, String[] args) {
        Location loc = player.getLocation();
        int newId = 0;
        for (Merchant m : MerchantDAO.getInstance().createQuery()) {
            newId++;
        }
        Merchant merchant = new Merchant(newId + 1);
        merchant.location = PrisonCommands.locationToString(loc);
        MerchantDAO.getInstance().save(merchant);
        MerchantEntity.loadMerchant(loc, merchant.id);
        MessageFormatter.sendSuccessMessage(player, "Merchant spawned and saved.");
    }

    public static void stats(Player player, String[] args) {
        PrisonerSessionData psd = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        player.sendMessage(ChatColor.GOLD + "======= " + ChatColor.RED + "Prisoner Stats" + ChatColor.GOLD + " =======");
        player.sendMessage(ChatColor.GOLD + "Blocks broken: " + RanksCommands.formatNumber(psd.pickData.blocksMined));
        player.sendMessage(ChatColor.GOLD + "==========================");
    }
}

package net.nyxcraft.dev.prison.command;

import java.util.UUID;

import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.api.PrisonerDataAPI;
import net.nyxcraft.dev.prison.database.entities.Cell;
import net.nyxcraft.dev.prison.menu.CellMenu;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.entity.Player;

public class CellCommands {

    public CellCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "cell", CellCommands::cellCommand);
    }

    public static void cellCommand(Player player, String[] args) {
        CellMenu menu = new CellMenu();
        menu.setPlayer(player);
        menu.openMenu(player);
    }

    public static Cell claimNewCell(Player p) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(p.getUniqueId());
        Cell cell = getNextCell(p.getUniqueId(), PrisonDatabaseManager.getPrisonData().currentCellId);
        prisoner.cells.add(cell);
        PrisonerDataAPI.addCell(prisoner, cell);
        return cell;
    }

    public static Cell getNextCell(UUID uuid, int current) {
        int option = 0;
        int length = 1;
        int count = 0;
        int times = 0;
        int x = 0;
        int z = 0;
        for (int i = 0; i < current; i++) {
            switch (option) {
                case 0:
                    x--;
                    break;
                case 1:
                    z += 2;
                    break;
                case 2:
                    x++;
                    break;
                case 3:
                    z -= 2;
                    break;
            }
            count++;
            if (count == length) {
                times++;
                count = 0;
                option++;
                if (option == 4) {
                    option = 0;
                }
                if (times == 2) {
                    times = 0;
                    length++;
                }
            }
        }

        return new Cell(uuid, x, z);
    }
}

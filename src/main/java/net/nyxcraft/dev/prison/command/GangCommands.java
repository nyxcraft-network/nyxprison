package net.nyxcraft.dev.prison.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.api.GangAPI;
import net.nyxcraft.dev.prison.database.api.PrisonerDataAPI;
import net.nyxcraft.dev.prison.database.entities.Gang;
import net.nyxcraft.dev.prison.gang.GangManager;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class GangCommands {

    private static HashMap<UUID, String> invites = new HashMap<UUID, String>();
    private static HashMap<UUID, Long> lastInvite = new HashMap<UUID, Long>();
    private static ArrayList<String> chatters = new ArrayList<>();

    public GangCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "gang", GangCommands::gangCommand);
        CommandRegistry.registerPlayerSubCommand("gang", "help", GangCommands::gangHelp);
        CommandRegistry.registerPlayerSubCommand("gang", "create", GangCommands::gangCreate);
        CommandRegistry.registerPlayerSubCommand("gang", "disband", GangCommands::gangDisband);
        CommandRegistry.registerPlayerSubCommand("gang", "leave", GangCommands::gangLeave);
        CommandRegistry.registerPlayerSubCommand("gang", "toggle", GangCommands::gangToggle);
        CommandRegistry.registerPlayerSubCommand("gang", "members", GangCommands::gangMembers);
        CommandRegistry.registerPlayerSubCommand("gang", "invite", GangCommands::gangInvite);
        CommandRegistry.registerPlayerSubCommand("gang", "join", GangCommands::gangJoin);
        CommandRegistry.registerPlayerSubCommand("gang", "chat", GangCommands::gangChat);
    }

    public static void gangCommand(Player player, String[] args) {
        gangHelp(player, args);
    }

    private static void gangHelp(Player player, String[] args) {
        player.sendMessage(ChatColor.RED + "/gang create");
        player.sendMessage(ChatColor.RED + "/gang disband");
        player.sendMessage(ChatColor.RED + "/gang leave");
        player.sendMessage(ChatColor.RED + "/gang members");
        player.sendMessage(ChatColor.RED + "/gang invite");
        player.sendMessage(ChatColor.RED + "/gang join");
        player.sendMessage(ChatColor.RED + "/gang toggle");
        player.sendMessage(ChatColor.RED + "/gang chat");
    }

    private static void gangCreate(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/gang create [name]");
            return;
        }

        if (prisoner.gang != null) {
            MessageFormatter.sendErrorMessage(player, "You already have a gang.");
            return;
        }

        Gang gang = GangManager.get(args[0].toLowerCase());
        if (gang != null) {
            MessageFormatter.sendErrorMessage(player, "That gang already exists.");
            return;
        }

        gang = new Gang(player.getUniqueId().toString(), args[0].toLowerCase());
        GangAPI.create(gang);
        prisoner.gang = gang.name;
        PrisonerDataAPI.setGang(prisoner);
        Bukkit.broadcastMessage(ChatColor.GOLD + player.getName() + " has created a new gang called " + args[0]);
    }

    private static void gangDisband(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (prisoner.gang == null) {
            MessageFormatter.sendErrorMessage(player, "You are not in a gang.");
            return;
        }

        Gang gang = GangManager.get(prisoner.gang);
        if (gang == null) {
            MessageFormatter.sendErrorMessage(player, "We could not find your gang.");
            return;
        }

        if (!gang.leader.equalsIgnoreCase(prisoner.uuid.toString())) {
            MessageFormatter.sendErrorMessage(player, "Only the leader of your gang can do that.");
            return;
        }

        for (String member : gang.members) {
            if (Bukkit.getPlayer(UUID.fromString(member)) != null) {
                PrisonerSessionData sessionData = NyxPrison.getInstance().getPrisonerSessionData(UUID.fromString(member));
                GangManager.removeMember(sessionData.gang, UUID.fromString(member));
                sessionData.gang = null;
                MessageFormatter.sendSuccessMessage(player, "Your gang has been disbanded.");
            }

            PrisonerDataAPI.removeGang(UUID.fromString(member));
        }

        GangManager.remove(prisoner.gang);
        GangAPI.disband(prisoner.gang);
        prisoner.gang = null;
        player.sendMessage(ChatColor.RED + "Your gang has been disbanded. Oh no...");
    }

    private static void gangLeave(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (prisoner.gang == null) {
            MessageFormatter.sendErrorMessage(player, "You are not in a gang.");
            return;
        }

        Gang gang = GangManager.get(prisoner.gang);
        if (gang == null) {
            MessageFormatter.sendErrorMessage(player, "We could not find your gang.");
            return;
        }

        if (gang.leader.equalsIgnoreCase(prisoner.uuid.toString())) {
            MessageFormatter.sendErrorMessage(player, "You are the leader of this gang. You can only disband the gang.");
            return;
        }

        GangManager.removeMember(prisoner.gang, player.getUniqueId());

        for (String string : gang.members) {
            if (Bukkit.getPlayer(UUID.fromString(string)) != null) {
                MessageFormatter.sendGeneralMessage(Bukkit.getPlayer(UUID.fromString(string)), player.getName() + " has left your gang.");
            }
        }

        if (Bukkit.getPlayer(UUID.fromString(gang.leader)) != null) {
            MessageFormatter.sendGeneralMessage(Bukkit.getPlayer(UUID.fromString(gang.leader)), player.getName() + " has left your gang.");
        }

        GangAPI.removeMember(player.getUniqueId(), gang);
        prisoner.gang = null;
        MessageFormatter.sendSuccessMessage(player, "You have left your gang.");
        PrisonerDataAPI.removeGang(prisoner.uuid);
    }

    private static void gangToggle(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (prisoner.gang == null) {
            MessageFormatter.sendErrorMessage(player, "You are not in a gang.");
            return;
        }

        Gang gang = GangManager.get(prisoner.gang);
        if (gang == null) {
            MessageFormatter.sendErrorMessage(player, "We could not find your gang.");
            return;
        }

        if (!gang.leader.equalsIgnoreCase(prisoner.uuid.toString())) {
            MessageFormatter.sendErrorMessage(player, "Only the leader of your gang can do that.");
            return;
        }

        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/gang toggle [option]", "Options: friendlyFire, inviteOnly, freeInvite");
            return;
        }

        boolean updated = false;
        if (args[0].equalsIgnoreCase("friendlyFire")) {
            gang.gangSettings.friendlyFire = !gang.gangSettings.friendlyFire;
            if (gang.gangSettings.friendlyFire) {
                MessageFormatter.sendSuccessMessage(player, "Toggled friendly fire on");
            } else {
                MessageFormatter.sendSuccessMessage(player, "Toggled friendly fire off");
            }
            updated = true;
        } else if (args[0].equalsIgnoreCase("inviteOnly")) {
            gang.gangSettings.inviteOnly = !gang.gangSettings.inviteOnly;
            if (gang.gangSettings.inviteOnly) {
                MessageFormatter.sendSuccessMessage(player, "Toggled invite only on");
            } else {
                MessageFormatter.sendSuccessMessage(player, "Toggled invite only off");
            }
            updated = true;
        } else if (args[0].equalsIgnoreCase("freeInvite")) {
            gang.gangSettings.freeInvite = !gang.gangSettings.freeInvite;
            if (gang.gangSettings.freeInvite) {
                MessageFormatter.sendSuccessMessage(player, "Toggled free invite on");
            } else {
                MessageFormatter.sendSuccessMessage(player, "Toggled free invite off");
            }
        } else {
            MessageFormatter.sendErrorMessage(player, "The available options are: friendlyFire, inviteOnly, freeInvite");
            return;
        }

        if (updated) {
            GangAPI.updateSettings(gang);
        }
    }

    private static void gangMembers(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (prisoner.gang == null) {
            MessageFormatter.sendErrorMessage(player, "You are not in a gang.");
            return;
        }

        Gang gang = GangManager.get(prisoner.gang);
        if (gang == null) {
            MessageFormatter.sendErrorMessage(player, "We could not find your gang.");
            return;
        }

        MessageFormatter.sendInfoMessage(player, "Leader: " + Bukkit.getOfflinePlayer(UUID.fromString(gang.leader)).getName());
        for (String member : gang.members) {
            if (!member.equals(gang.leader)) {
                MessageFormatter.sendGeneralMessage(player, "Member: " + Bukkit.getOfflinePlayer(UUID.fromString(member)).getName());
            }
        }
    }

    private static void gangInvite(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/gang invite [player]");
            return;
        }

        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (prisoner.gang == null) {
            MessageFormatter.sendErrorMessage(player, "You are not in a gang.");
            return;
        }

        Gang gang = GangManager.get(prisoner.gang);
        if (gang == null) {
            MessageFormatter.sendErrorMessage(player, "We could not find your gang.");
            return;
        }

        if (!gang.gangSettings.freeInvite && !gang.leader.equals(player.getUniqueId().toString())) {
            MessageFormatter.sendErrorMessage(player, "Only the leader of your gang can do that.");
            return;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            MessageFormatter.sendErrorMessage(player, "Player not found.");
            return;
        }

        if (lastInvite.containsKey(player.getUniqueId())) {
            if (System.currentTimeMillis() - lastInvite.get(player.getUniqueId()) <= 5000) {
                MessageFormatter.sendErrorMessage(player, "Do not spam invites.");
                lastInvite.put(player.getUniqueId(), System.currentTimeMillis());
            }
        }

        lastInvite.put(player.getUniqueId(), System.currentTimeMillis());
        MessageFormatter.sendSuccessMessage(player, target.getName() + " has been invited.");
        MessageFormatter.sendGeneralMessage(target, player.getName() + " has invited you to " + gang.casedName + ". Use /gang join " + gang.casedName + " to accept.");
        invites.put(target.getUniqueId(), gang.name);
    }

    private static void gangJoin(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/gang join [gang]");
            return;
        }

        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (prisoner.gang != null) {
            MessageFormatter.sendErrorMessage(player, "You are in a gang already.");
            return;
        }

        Gang gang = GangManager.get(args[0].toLowerCase());
        if (gang == null) {
            MessageFormatter.sendErrorMessage(player, "We could not find your gang.");
            return;
        }

        if (gang.gangSettings.inviteOnly) {
            if (invites.get(player.getUniqueId()) == null || !invites.get(player.getUniqueId()).equalsIgnoreCase(gang.name)) {
                MessageFormatter.sendErrorMessage(player, "That gang is invite only.");
                return;
            }
        }

        for (String string : gang.members) {
            if (Bukkit.getPlayer(UUID.fromString(string)) != null) {
                MessageFormatter.sendGeneralMessage(Bukkit.getPlayer(UUID.fromString(string)), player.getName() + " has joined your gang.");
            }
        }

        if (Bukkit.getPlayer(UUID.fromString(gang.leader)) != null) {
            MessageFormatter.sendGeneralMessage(Bukkit.getPlayer(UUID.fromString(gang.leader)), player.getName() + " has joined your gang.");
        }

        MessageFormatter.sendSuccessMessage(player, "You have joined the gang " + gang.casedName + "!");
        gang.members.add(prisoner.uuid.toString());
        GangAPI.addMember(player.getUniqueId(), gang);
        GangManager.addMember(gang.name, player.getUniqueId());
        prisoner.gang = gang.name;
        PrisonerDataAPI.setGang(prisoner);
    }

    private static void gangChat(Player player, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        if (prisoner.gang == null) {
            MessageFormatter.sendErrorMessage(player, "You are not in a gang.");
            return;
        }

        if (chatters.contains(player.getName())) {
            chatters.remove(player.getName());
            MessageFormatter.sendSuccessMessage(player, "Turned off gang chat.");
        } else {
            chatters.add(player.getName());
            MessageFormatter.sendSuccessMessage(player, "Turned on gang chat.");
        }
    }

    public static ArrayList<String> getChatters() {
        return chatters;
    }

}

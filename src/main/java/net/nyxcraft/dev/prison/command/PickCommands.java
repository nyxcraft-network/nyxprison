package net.nyxcraft.dev.prison.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.utils.UUIDFetcher;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.api.PickDataAPI;
import net.nyxcraft.dev.prison.database.entities.PickData;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PickCommands {

    public PickCommands() {
        CommandRegistry.registerUniversalCommand(NyxPrison.getInstance(), "pick", Rank.ADMINISTRATOR, PickCommands::help);
        CommandRegistry.registerPlayerSubCommand("pick", "levelup", Rank.ADMINISTRATOR, PickCommands::levelUp);
        CommandRegistry.registerUniversalSubCommand("pick", "addupgrade", Rank.ADMINISTRATOR, PickCommands::addUpgrade);
        CommandRegistry.registerUniversalSubCommand("pick", "respec", Rank.ADMINISTRATOR, PickCommands::respec);
    }

    public static void help(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.RED + "/pick levelup");
        sender.sendMessage(ChatColor.RED + "/pick addupgrade <quanity> <quantum:true|false>");
    }

    public static void levelUp(Player sender, String[] args) {
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(sender.getUniqueId());
        prisoner.pickData.exp = 0;
        prisoner.pickData.level += 1;
        prisoner.pickData.upgrades += 1;
        prisoner.update();
        MessageFormatter.sendSuccessMessage(sender, "Your pickaxe has leveled up.");
        PickDataAPI.savePickData(prisoner);
    }

    @SuppressWarnings("serial")
    public static void addUpgrade(CommandSender sender, String[] args) {
        if (args.length != 3) {
            MessageFormatter.sendUsageMessage(sender, "/pick addupgrade <player> <quantity> <quantum:true|false>");
        }

        Integer q = null;
        Boolean qq = null;
        try {
            q = Integer.parseInt(args[1]);
            qq = Boolean.parseBoolean(args[2]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendErrorMessage(sender, "Insufficient parameters.");
            return;
        }

        final String name = args[0];
        final int quantity = q.intValue();
        final boolean quantum = qq.booleanValue();
        Player player = Bukkit.getPlayer(name);
        PrisonerData prisoner;
        PickData data = player != null ? NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId()).pickData : (prisoner = PrisonDatabaseManager.getPrisonerData(name)) == null ? null: prisoner.pickData;

        if (data != null) {
            if (quantum) {
                data.quantumUpgrades += quantity;
            } else {
                data.upgrades += quantity;
            }
            PickDataAPI.savePickData(data);
        } else {
            Bukkit.getScheduler().runTaskAsynchronously(NyxCore.getInstance(), () -> {
                List<String> names = new ArrayList<String>() {{
                    add(name);
                }};
                UUIDFetcher fetcher = new UUIDFetcher(names);
                Map<String, UUID> uniqueIds = null;

                try {
                    uniqueIds = fetcher.call();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                if (uniqueIds == null || uniqueIds.isEmpty()) {
                    return;
                }

                for (Map.Entry<String, UUID> entry : uniqueIds.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase(args[0])) {
                        PrisonerData p = PrisonDatabaseManager.createPrisonerData(entry.getValue(), entry.getKey());
                        if (quantum) {
                            p.pickData.quantumUpgrades += quantity;
                        } else {
                            p.pickData.upgrades += quantity;
                        }
                        PickDataAPI.savePickUpgrades(entry.getValue(), p.pickData);
                        return;
                    }
                }
            });
        }
    }

    public static void respec(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/pick respec <player>");
        }

        final String name = args[0];
        Player player = Bukkit.getPlayer(name);

        if (player != null) {
            PrisonerSessionData data = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
            data.pickData.enchantments.clear();
            PickDataAPI.savePickEnchants(data);
            data.update();
        } else {
            Bukkit.getScheduler().runTaskAsynchronously(NyxCore.getInstance(), () -> {
                List<String> names = new ArrayList<String>() {
                    private static final long serialVersionUID = 1L;
                {
                    add(name);
                }};
                UUIDFetcher fetcher = new UUIDFetcher(names);
                Map<String, UUID> uniqueIds = null;

                try {
                    uniqueIds = fetcher.call();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                if (uniqueIds == null || uniqueIds.isEmpty()) {
                    return;
                }

                for (Map.Entry<String, UUID> entry : uniqueIds.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase(args[0])) {
                        PrisonerData p = PrisonDatabaseManager.initPrisonerData(entry.getValue(), entry.getKey());
                        if (p.pickData.enchantments.size() > 0) {
                            PickDataAPI.savePickEnchants(entry.getValue(), p.pickData);
                        }
                        return;
                    }
                }
            });
        }
    }

}

package net.nyxcraft.dev.prison.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.dao.MineDAO;
import net.nyxcraft.dev.prison.database.entities.Mine;
import net.nyxcraft.dev.prison.menu.MineMenu;
import net.nyxcraft.dev.prison.player.PrisonRank;
import net.nyxcraft.dev.prison.world.MineSessionData;
import net.nyxcraft.dev.prison.world.WorldEditUtils;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;

public class MineCommands {

    public MineCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "mine", MineCommands::mineCommand);
        CommandRegistry.registerPlayerSubCommand("mine", "help", Rank.ADMINISTRATOR, MineCommands::mineHelp);
        CommandRegistry.registerPlayerSubCommand("mine", "create", Rank.ADMINISTRATOR, MineCommands::mineCreate);
        CommandRegistry.registerPlayerSubCommand("mine", "edit", Rank.ADMINISTRATOR, MineCommands::mineEdit);
        CommandRegistry.registerPlayerSubCommand("mine", "setrank", Rank.ADMINISTRATOR, MineCommands::mineSetRank);
        CommandRegistry.registerPlayerSubCommand("mine", "reset", Rank.ADMINISTRATOR, MineCommands::mineReset);
        CommandRegistry.registerPlayerSubCommand("mine", "delete", Rank.ADMINISTRATOR, MineCommands::mineDelete);
        CommandRegistry.registerPlayerSubCommand("mine", "list", Rank.ADMINISTRATOR, MineCommands::mineList);
        CommandRegistry.registerPlayerSubCommand("mine", "info", Rank.ADMINISTRATOR, MineCommands::mineInfo);
        CommandRegistry.registerPlayerSubCommand("mine", "setwarp", Rank.ADMINISTRATOR, MineCommands::mineSetWarp);
        CommandRegistry.registerPlayerSubCommand("mine", "reload", Rank.ADMINISTRATOR, MineCommands::mineReload);
    }

    public static void mineCommand(Player player, String[] args) {
        if (args.length == 0) {
            MineMenu menu = new MineMenu();
            menu.setPlayer(player);
            menu.openMenu(player);
        } else {
            Mine mine = PrisonDatabaseManager.getMine(args[0]);
            if (mine == null) {
                MineMenu menu = new MineMenu();
                menu.setPlayer(player);
                menu.openMenu(player);
                MessageFormatter.sendErrorMessage(player, "Mine not found.");
            } else {
                player.teleport(PrisonCommands.stringToLocation(mine.warpLocation));
                MessageFormatter.sendSuccessMessage(player, "Teleported to mine");
            }
        }

    }

    public static void mineHelp(Player player, String[] args) {
        player.sendMessage(ChatColor.RED + "/mine create [mine]");
        player.sendMessage(ChatColor.RED + "/mine edit [mine] [+/-] [ore] [percent]");
        player.sendMessage(ChatColor.RED + "/mine setrank [mine] [rank]");
        player.sendMessage(ChatColor.RED + "/mine setwarp [mine]");
        player.sendMessage(ChatColor.RED + "/mine reset [mine]");
        player.sendMessage(ChatColor.RED + "/mine delete [mine]");
        player.sendMessage(ChatColor.RED + "/mine info [mine]");
        player.sendMessage(ChatColor.RED + "/mine list [mine]");
        player.sendMessage(ChatColor.RED + "/mine reload");
    }

    public static void mineCreate(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/mine create [name]");
            return;
        }

        Mine mine = PrisonDatabaseManager.getMine(args[0].toLowerCase());
        if (mine != null) {
            MessageFormatter.sendErrorMessage(player, "That mine already exists.");
            return;
        }

        CuboidSelection cube = WorldEditUtils.getSelection(player);
        if (cube == null) {
            MessageFormatter.sendErrorMessage(player, "You need to make a selection first");
            return;
        }

        mine = new Mine(args[0].toLowerCase());
        Location loc1 = cube.getMaximumPoint();
        Location loc2 = cube.getMinimumPoint();
        mine.minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
        mine.minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
        mine.minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
        mine.maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
        mine.maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
        mine.maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
        mine.world = loc1.getWorld().getName();
        mine.ores = "";
        mine.casedName = args[0];

        MineDAO.getInstance().save(mine);
        MessageFormatter.sendSuccessMessage(player, "You have created a new mine called " + args[0]);
    }

    public static void mineEdit(Player player, String[] args) {

        if (args.length != 4) {
            MessageFormatter.sendUsageMessage(player, "/mine edit [mine] [+/-] [ore] [percent]");
            return;
        }

        Mine mine = PrisonDatabaseManager.getMine(args[0].toLowerCase());
        if (mine == null) {
            MessageFormatter.sendErrorMessage(player, "That mine does not exist.");
            return;
        }

        if (!(args[1].equals("+") || args[1].equals("-"))) {
            MessageFormatter.sendUsageMessage(player, "/mine edit [mine] [+/-] [ore] [percent]");
            return;
        }

        String[] values = args[2].split(":");

        if (values.length > 2) {
            MessageFormatter.sendErrorMessage(player, "Item ids are id:data");
            return;
        }

        int material = 0, value = 0;

        try {
            material = Integer.parseInt(values[0]);
        } catch (NumberFormatException ex) {
            MessageFormatter.sendErrorMessage(player, "Ids must be a number.");
            return;
        }

        if (material > 192) {
            MessageFormatter.sendErrorMessage(player, "That is not a valid block.");
            return;
        }

        if (values.length == 2) {
            try {
                value = Integer.parseInt(values[1]);
            } catch (NumberFormatException ex) {
                MessageFormatter.sendErrorMessage(player, "Item data must be a number");
                return;
            }
        }

        Double percent = 0.0;
        try {
            percent = Double.parseDouble(args[3]);
        } catch (NumberFormatException ex) {
            MessageFormatter.sendErrorMessage(player, "That is not a number.");
            return;
        }

        List<MineBlockData> mineBlockData = stringToMineBlockData(mine.ores);
        MineBlockData currentData = new MineBlockData(material, value, percent);

        if (args[1].equals("+")) {

            double totalPercent = 0;
            for (MineBlockData data : mineBlockData) {
                totalPercent += data.percent;
            }

            if (totalPercent + percent > 100) {
                MessageFormatter.sendErrorMessage(player, "The mine percent can't go over 100%");
                return;
            }

            mineBlockData = MineBlockData.add(mineBlockData, currentData);
            MessageFormatter.sendSuccessMessage(player, "Block added.");
        } else {
            mineBlockData = MineBlockData.subtract(mineBlockData, currentData);
            MessageFormatter.sendSuccessMessage(player, "Block subtracted.");
        }
        mine.ores = mineBlockDataToString(mineBlockData);
        MineDAO.getInstance().save(mine);
    }

    public static void mineSetRank(Player player, String[] args) {

        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(player, "/mine setrank [mine] [rank]");
            return;
        }

        Mine mine = PrisonDatabaseManager.getMine(args[0].toLowerCase());

        if (mine == null) {
            MessageFormatter.sendErrorMessage(player, "That mine does not exist.");
            return;
        }

        PrisonRank rank = PrisonRank.valueOf(args[1]);

        if (rank == null) {
            MessageFormatter.sendErrorMessage(player, "That prison rank does not exist.");
            return;
        }

        mine.requiredPrisonRank = rank.name;
        MineDAO.getInstance().save(mine);

        MessageFormatter.sendGeneralMessage(player, mine.casedName + "'s required rank is now " + rank.name());
    }

    public static void mineReset(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/mine reset [mine]");
            return;
        }

        Mine mine = PrisonDatabaseManager.getMine(args[0].toLowerCase());
        if (mine == null) {
            MessageFormatter.sendErrorMessage(player, "That mine does not exist.");
            return;
        }

        mine.reset();
        MessageFormatter.sendGeneralMessage(player, mine.casedName + " mine has been reset.");
        MineSessionData msd = NyxPrison.getInstance().getMine(mine.name);
        NyxPrison.getInstance().getMines().add(new MineSessionData(mine, 0, msd.getNormalSize()));
    }

    public static void mineDelete(Player player, String[] args) {

        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/mine delete [mine]");
            return;
        }

        Mine mine = PrisonDatabaseManager.getMine(args[0].toLowerCase());

        if (mine == null) {
            MessageFormatter.sendErrorMessage(player, "That mine does not exist.");
            return;
        }

        MineDAO.getInstance().delete(mine);
        MessageFormatter.sendSuccessMessage(player, mine.casedName + " has been deleted");
    }

    @SuppressWarnings("deprecation")
    public static void mineInfo(Player player, String[] args) {

        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/mine info [mine]");
            return;
        }

        Mine mine = PrisonDatabaseManager.getMine(args[0].toLowerCase());

        if (mine == null) {
            MessageFormatter.sendErrorMessage(player, "That mine does not exist.");
            return;
        }
        List<MineBlockData> blockData = stringToMineBlockData(mine.ores);
        MessageFormatter.sendGeneralMessage(player, "Mine data for " + mine.casedName);
        for (MineBlockData data : blockData) {
            MessageFormatter.sendGeneralMessage(player, "Block: " + ChatColor.RED + StringUtils.capitaliseAllWords(Material.getMaterial(data.id).toString().replaceAll("_", " "))
                    + ChatColor.GOLD + " Data: " + ChatColor.RED + data.data + ChatColor.GOLD + " Percent: " + ChatColor.RED + data.percent);
        }

    }

    public static void mineList(Player player, String[] args) {
        for (Mine mine : MineDAO.getInstance().createQuery()) {
            MessageFormatter.sendInfoMessage(player, mine.casedName);
        }
    }

    public static void mineSetWarp(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/mine setwarp [mine]");
            return;
        }
        Mine mine = PrisonDatabaseManager.getMine(args[0].toLowerCase());
        if (mine == null) {
            MessageFormatter.sendErrorMessage(player, "That mine does not exist.");
            return;
        }
        mine.warpLocation = PrisonCommands.locationToString(player.getLocation());
        MineDAO.getInstance().save(mine);
        MessageFormatter.sendSuccessMessage(player, "Mine warp set!");
    }

    public static void mineReload(Player player, String[] args) {
        NyxPrison.getInstance().loadMines();
        MessageFormatter.sendSuccessMessage(player, "Mines successfully reloaded.");
    }

    public static class MineBlockData {
        public int id;
        public int data;
        public double percent;

        public MineBlockData(int id, int data, double percent) {
            this.id = id;
            this.data = data;
            this.percent = percent;
        }

        public static List<MineBlockData> subtract(List<MineBlockData> dataList, MineBlockData data) {
            MineBlockData toBeRemoved = null;
            for (MineBlockData listedData : dataList) {
                if (listedData.id == data.id && listedData.data == data.data) {
                    if (listedData.percent < data.percent) {
                        toBeRemoved = listedData;
                    } else {
                        listedData.percent -= data.percent;
                    }
                }
            }
            if (toBeRemoved != null) {
                dataList.remove(toBeRemoved);
            }
            return dataList;
        }

        public static List<MineBlockData> add(List<MineBlockData> dataList, MineBlockData data) {
            boolean exists = false;
            for (MineBlockData listedData : dataList) {
                if (listedData.id == data.id && listedData.data == data.data) {
                    listedData.percent += data.percent;
                    exists = true;
                }
            }

            if (!exists) {
                dataList.add(data);
            }
            return dataList;
        }
    }

    public static String mineBlockDataToString(Collection<MineBlockData> blocks) {
        String string = "";
        for (MineBlockData mineBlock : blocks) {
            string = string + mineBlock.id + "," + mineBlock.data + "," + mineBlock.percent + ";";
        }

        return string;
    }

    public static List<MineBlockData> stringToMineBlockData(String string) {
        List<MineBlockData> dataBlocks = new ArrayList<>();
        if (string.equals("")) {
            return dataBlocks;
        }
        String[] pieces = string.split(";");
        for (String piece : pieces) {
            String[] dataPieces = piece.split(",");
            dataBlocks.add(new MineBlockData(Integer.parseInt(dataPieces[0]), Integer.parseInt(dataPieces[1]), Double.parseDouble(dataPieces[2])));
        }
        return dataBlocks;
    }

}

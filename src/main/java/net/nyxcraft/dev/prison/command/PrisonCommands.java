package net.nyxcraft.dev.prison.command;

import java.util.Map;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.world.SerializeableLocation;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.dao.PrisonDataDAO;
import net.nyxcraft.dev.prison.database.entities.PrisonData;
import net.nyxcraft.dev.prison.menu.WarpMenu;
import net.nyxcraft.dev.prison.menu.WarpMenu.WarpData;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class PrisonCommands {

    public PrisonCommands() {
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "setspawn", Rank.ADMINISTRATOR, PrisonCommands::setSpawn);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "spawn", PrisonCommands::spawn);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "warp", PrisonCommands::warp);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "setwarp", Rank.ADMINISTRATOR, PrisonCommands::setWarp);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "setshopspawn", Rank.ADMINISTRATOR, PrisonCommands::setShopSpawn);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "shops", PrisonCommands::shops);
        CommandRegistry.registerPlayerCommand(NyxPrison.getInstance(), "shop", PrisonCommands::shops);
    }

    public static void spawn(Player player, String[] args) {
        player.teleport(NyxPrison.getInstance().getData().getSpawn());
        MessageFormatter.sendSuccessMessage(player, "You have teleported to spawn.");
    }

    public static void setSpawn(Player player, String[] args) {
        PrisonData prisonData = NyxPrison.getInstance().getData();
        prisonData.spawn = new SerializeableLocation(player.getLocation());
        PrisonDataDAO.getInstance().save(prisonData);
        MessageFormatter.sendSuccessMessage(player, "The spawn has been set.");
    }

    public static void setShopSpawn(Player player, String[] args) {
        PrisonData prisonData = NyxPrison.getInstance().getData();
        prisonData.shopSpawn = new SerializeableLocation(player.getLocation());
        PrisonDataDAO.getInstance().save(prisonData);
        MessageFormatter.sendSuccessMessage(player, "The shop spawn has been set.");
    }

    public static void shops(Player player, String[] args) {
        if (NyxPrison.getInstance().getData().shopSpawn == null) {
            MessageFormatter.sendErrorMessage(player, "The shop spawn is not configured.");
            return;
        }

        player.teleport(NyxPrison.getInstance().getData().shopSpawn.getLocation());
        MessageFormatter.sendSuccessMessage(player, "You have teleported to the shops.");
    }

    public static void warp(Player player, String[] args) {
        if (WarpMenu.getWarpMenu() == null) {
            new WarpMenu();
        }
        if (args.length == 0) {
            WarpMenu.getWarpMenu().openMenu(player);
        } else {
            Location loc = NyxPrison.getInstance().warps.get(args[0]).loc;
            if (loc == null) {
                WarpMenu.getWarpMenu().openMenu(player);
                MessageFormatter.sendErrorMessage(player, "Warp not found.");
            } else {
                player.teleport(loc);
                MessageFormatter.sendSuccessMessage(player, "Warped to " + args[0] + ".");
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static void setWarp(Player player, String[] args) {
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(player, "/setwarp [warp] [item]");
            return;
        }

        PrisonData pd = PrisonDatabaseManager.getPrisonData();

        // warp items
        Map<String, Material> warpItems = pd.warpItems;
        int material = 0;
        try {
            material = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            MessageFormatter.sendErrorMessage(player, "Ids must be a number.");
            return;
        }
        Material mat = Material.getMaterial(material);
        warpItems.put(args[0], mat);
        pd.warpItems = warpItems;

        // warp locations
        Map<String, String> warps = pd.warps;
        String loc = locationToString(player.getLocation());
        warps.put(args[0], loc);
        pd.warps = warps;

        PrisonDataDAO.getInstance().save(pd);
        WarpData warpData = new WarpData(player.getLocation(), mat);
        NyxPrison.getInstance().warps.put(args[0], warpData);
        new WarpMenu();
        MessageFormatter.sendSuccessMessage(player, "Warp set.");
    }

    public static String locationToString(Location loc) {
        return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getYaw() + ";" + loc.getPitch();
    }

    public static Location stringToLocation(String str) {
        String[] strar = str.split(";");
        Location newLoc = new Location(Bukkit.getWorld(strar[0]), Double.valueOf(strar[1]).doubleValue(), Double.valueOf(strar[2]).doubleValue(), Double.valueOf(strar[3]).doubleValue(), Float.valueOf(strar[4]).floatValue(), Float.valueOf(strar[5])
                .floatValue());
        return newLoc.clone();
    }

}

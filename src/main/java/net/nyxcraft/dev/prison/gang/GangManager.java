package net.nyxcraft.dev.prison.gang;

import net.nyxcraft.dev.nyxutils.collections.CaseInsensitiveHashMap;
import net.nyxcraft.dev.prison.database.dao.GangDAO;
import net.nyxcraft.dev.prison.database.entities.Gang;

import java.util.UUID;

public class GangManager {

    private static CaseInsensitiveHashMap<Gang> gangs = new CaseInsensitiveHashMap<>();

    public static boolean exists(String gang) {
        return gangs.containsKey(gang);
    }

    public static Gang get(String gang) {
        if (exists(gang)) {
            return gangs.get(gang);
        }

        Gang g = fetch(gang);
        if (g != null) {
            return store(g);
        }

        return null;
    }

    public static Gang fetch(String gang) {
        return GangDAO.getInstance().findOne("name", gang.toLowerCase());
    }

    public static Gang store(Gang gang) {
        gangs.put(gang.name, gang);
        return gang;
    }

    public static Gang remove(String gang) {
        return gangs.remove(gang);
    }

    public static void addMember(String gang, UUID uuid) {
        if (exists(gang)) {
            get(gang).members.add(uuid.toString());
        }
    }

    public static void removeMember(String gang, UUID uuid) {
        if (exists(gang)) {
            get(gang).members.remove(uuid.toString());
        }
    }

}

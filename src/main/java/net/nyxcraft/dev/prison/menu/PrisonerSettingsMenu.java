package net.nyxcraft.dev.prison.menu;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import net.nyxcraft.dev.nyxcore.utils.Utils;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.entities.PrisonerSettings;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class PrisonerSettingsMenu extends Menu {
    private static Map<UUID, PrisonerSettingsMenu> users = new HashMap<>();
    private PrisonerSettings settings;

    private MenuItem pvp;

    public PrisonerSettingsMenu(PrisonerSessionData data) {
        super(ChatColor.RED + "User Preferences", 6);
        this.settings = data.prisonerSettings;
        users.put(data.uuid, this);
        init();
    }

    @SuppressWarnings("deprecation")
    public void init() {
        DyeColor visDye = settings.isPvpEnabled() ? DyeColor.LIME : DyeColor.GRAY;
        pvp = new MenuItem(ChatColor.GOLD + PrisonerSettingsMenuEnum.TOGGLE_PVP.getSetting(), new MaterialData(Material.INK_SACK), visDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                boolean canToggle = (System.currentTimeMillis() - settings.getPvpLastToggled()) > 1000 * 60 * 10;
                if (canToggle) {
                    if (settings.isPvpEnabled()) {
                        pvp.setData(DyeColor.GRAY.getDyeData());
                        settings.setPvpEnabled(false);
                    } else {
                        pvp.setData(DyeColor.LIME.getDyeData());
                        settings.setPvpEnabled(true);
                    }

                    settings.setPvpLastToggled(System.currentTimeMillis());
                    PrisonerSettingsMenu.this.updateInventory();
                } else {
                    MessageFormatter.sendErrorMessage(player, "You must wait " + Utils.convertToFormattedTime((settings.getPvpLastToggled() + 1000 * 60 * 10) - System.currentTimeMillis()) + " before you can toggle pvp.");
                }
            }
        };

        this.addMenuItem(PrisonerSettingsMenuEnum.TOGGLE_PVP.getMenuItem(), PrisonerSettingsMenuEnum.TOGGLE_PVP.getIndex());
        this.addMenuItem(pvp, PrisonerSettingsMenuEnum.TOGGLE_PVP.getIndex() + 9);
        updateInventory();
    }

    public static PrisonerSettingsMenu getUserSettings(Player player) {
        PrisonerSettingsMenu menu = users.containsKey(player.getUniqueId()) ? users.get(player.getUniqueId()) : new PrisonerSettingsMenu(NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId()));
        users.put(player.getUniqueId(), menu);
        return menu;
    }

    public static void cleanup(Player player) {
        users.remove(player.getUniqueId());
    }
}

enum PrisonerSettingsMenuEnum {

    TOGGLE_PVP("Prisoner PVP", Material.IRON_SWORD, 10);

    private String setting;
    private Material item;
    private int index;

    PrisonerSettingsMenuEnum(String setting, Material item, int index) {
        this.setting = setting;
        this.item = item;
        this.index = index;
    }

    public String getSetting() {
        return setting;
    }

    public Material getItem() {
        return item;
    }

    public int getIndex() {
        return index;
    }

    public MenuItem getMenuItem() {
        return new MenuItem(ChatColor.GOLD + setting, new MaterialData(item)) {
            @Override
            public void onClick(Player player) {
                //
            }
        };
    }
}
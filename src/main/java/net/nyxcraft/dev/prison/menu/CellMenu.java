package net.nyxcraft.dev.prison.menu;

import java.util.ArrayList;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.command.CellCommands;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.entities.Cell;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class CellMenu extends Menu {

    private Player p;

    public CellMenu() {
        super(ChatColor.GOLD.toString() + ChatColor.BOLD + "Prison Cells", 1);
    }

    public void setPlayer(Player p) {
        this.p = p;
        init();
    }

    public void init() {
        PrisonerData prisoner = PrisonDatabaseManager.getPrisonerData(p.getUniqueId());
        ArrayList<MenuItem> currentCells = new ArrayList<MenuItem>();
        for (Cell cell : prisoner.cells) {
            currentCells.add(new MenuItem(ChatColor.YELLOW.toString() + ChatColor.ITALIC + "Cell #" + currentCells.size(), new MaterialData(Material.IRON_FENCE)) {
                @Override
                public void onClick(Player player) {
                    cell.teleportToCell(player);
                }
            });
        }

        @SuppressWarnings("deprecation")
        MenuItem newCell = new MenuItem(ChatColor.GREEN.toString() + ChatColor.BOLD + "New Cell", new MaterialData(Material.WOOL, (byte) 5)) {
            @Override
            public void onClick(Player player) {
                PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
                int cells = NyxCore.getUser(player.getUniqueId()).getRank().ordinal() * 2 + 2;
                if (cells > 9) {
                    cells = 9;
                }
                if (prisoner.cells.size() >= cells) {
                    MessageFormatter.sendErrorMessage(player, "You have the max amount of cells.");
                    return;
                }
                Cell cell = CellCommands.claimNewCell(player);
                NyxPrison.getInstance().getData().incrementCellId();
                cell.teleportToCell(player);
                player.closeInventory();
            }
        };
        int currentSpot = 0;
        for (MenuItem item : currentCells) {
            if (currentSpot >= 9) {
                continue;
            }
            addMenuItem(item, currentSpot);
            currentSpot++;
        }
        if (currentSpot < 9) {
            addMenuItem(newCell, currentSpot);
        }
    }
}

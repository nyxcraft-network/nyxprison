package net.nyxcraft.dev.prison.menu;

import java.util.ArrayList;
import java.util.Arrays;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import net.nyxcraft.dev.prison.command.RanksCommands;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;
import net.nyxcraft.dev.prison.economy.MerchantBlock;
import net.nyxcraft.dev.prison.utils.ItemNames;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

public class MerchantMenu extends Menu {

    public static ArrayList<MerchantMenu> merchantMenus = new ArrayList<>();

    public Player player;

    public MerchantMenu() {
        super(ChatColor.GOLD + "Merchant Shop", (int) Math.ceil((double) MerchantBlock.values().length / 9));
        merchantMenus.add(this);
    }

    public void setPlayer(Player player) {
        this.player = player;
        init();
    }

    @SuppressWarnings("deprecation")
    public void init() {
        PrisonerData prisoner = PrisonDatabaseManager.getPrisonerData(player.getUniqueId());
        int loc = 0;
        for (MerchantBlock block : MerchantBlock.values()) {
            String name = ItemNames.lookup(new ItemStack(block.id, 1, (short) 0, (byte) block.data));
            MenuItem item = new MenuItem(ChatColor.RED + name, new MaterialData(block.id, (byte) block.data)) {

                @Override
                public void onClick(Player player) {
                    if (prisoner.balance >= block.price) {
                        prisoner.balance -= block.price;
                        MessageFormatter.sendSuccessMessage(player, "Purchased " + name + " for $" + RanksCommands.formatNumber(block.price));
                        player.getInventory().addItem(new ItemStack(block.id, block.quantity, (short) 0, (byte) block.data));
                    } else {
                        MessageFormatter.sendErrorMessage(player, "You do not have enough money.");
                    }
                }
            };
            item.setDescriptions(Arrays.asList(new String[] { ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Price: $" + RanksCommands.formatNumber(block.price), ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Quantity: " + block.quantity }));
            this.addMenuItem(item, loc++);
        }
    }

    public static MerchantMenu getMerchantMenu(Player player) {
        for (MerchantMenu menu : merchantMenus) {
            if (menu.player == player) {
                return menu;
            }
        }
        MerchantMenu menu = new MerchantMenu();
        menu.setPlayer(player);
        return menu;
    }

}

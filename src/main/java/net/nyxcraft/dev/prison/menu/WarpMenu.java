package net.nyxcraft.dev.prison.menu;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import net.nyxcraft.dev.prison.NyxPrison;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class WarpMenu extends Menu {

    private static WarpMenu warpMenu;
    public int num = 0;

    public WarpMenu() {
        super(ChatColor.RED + "Warps", 1);
        init();
        warpMenu = this;
    }

    @Override
    public void openMenu(Player player) {
        int size = (int) Math.ceil((double) NyxPrison.getInstance().warps.size() / 9);
        if (size == 0) {
            size++;
        }
        if (super.getRows() != size) {
            super.setRows(size);
        }
        super.openMenu(player);
    }

    public void init() {
        for (String string : NyxPrison.getInstance().warps.keySet()) {
            addMenuItem(new MenuItem(ChatColor.YELLOW.toString() + ChatColor.ITALIC + string, new MaterialData(NyxPrison.getInstance().warps.get(string).mat)) {
                @Override
                public void onClick(Player player) {
                    MessageFormatter.sendSuccessMessage(player, "Warped to " + string + ".");
                    player.teleport(NyxPrison.getInstance().warps.get(string).loc);
                }
            }, num);
            num++;
        }
    }

    public static WarpMenu getWarpMenu() {
        return warpMenu;
    }

    public static class WarpData {
        public Location loc;
        public Material mat;

        public WarpData(Location loc, Material mat) {
            this.loc = loc;
            this.mat = mat;
        }
    }

}

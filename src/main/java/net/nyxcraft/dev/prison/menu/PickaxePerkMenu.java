package net.nyxcraft.dev.prison.menu;

import java.util.Arrays;
import java.util.Map;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.api.PickDataAPI;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class PickaxePerkMenu extends Menu {

    private PrisonerSessionData prisoner;

    public PickaxePerkMenu() {
        super(ChatColor.GOLD.toString() + ChatColor.BOLD + "Pick Upgrades", 2);
    }

    public void setPlayer(Player p) {
        this.prisoner = NyxPrison.getInstance().getPrisonerSessionData(p.getUniqueId());
        init();
    }

    public void init() {
        updateDisplay();
        MenuItem efficiency = new MenuItem(ChatColor.WHITE.toString() + ChatColor.ITALIC + "Efficiency", new MaterialData(Material.STONE)) {
            @Override
            public void onClick(Player player) {
                if (prisoner.pickData.availableUpgrades() >= 1) {
                    Map<Integer, Integer> enchants = prisoner.pickData.enchantments;
                    Integer level;
                    if ((level = enchants.get(32)) == null) {
                        enchants.put(32, 1);
                    } else {
                        if (level.intValue() <= 100) {
                            enchants.put(32, enchants.get(32) + 1);
                        } else {
                            MessageFormatter.sendErrorMessage(player, "You have reached the max level for this enchant.");
                            return;
                        }
                    }
                    prisoner.pickData.enchantments = enchants;
                    save(prisoner);
                    prisoner.update();
                    MessageFormatter.sendSuccessMessage(player, "Your pick's effeciency level has increased.");
                    updateDisplay();
                } else {
                    MessageFormatter.sendErrorMessage(player, "You do not have any pick upgrades available.");
                }
            }
        };
        efficiency.setDescriptions(Arrays.asList(new String[] { ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Add a level of efficiency.", ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Cost: 1", ChatColor.DARK_PURPLE + "Max Level: 100" }));
        MenuItem fortune = new MenuItem(ChatColor.WHITE.toString() + ChatColor.ITALIC + "Fortune", new MaterialData(Material.DIAMOND)) {
            @Override
            public void onClick(Player player) {
                if (prisoner.pickData.availableUpgrades() >= 1) {
                    Map<Integer, Integer> enchants = prisoner.pickData.enchantments;
                    Integer level;
                    if ((level = enchants.get(35)) == null) {
                        enchants.put(35, 1);
                    } else {
                        if (level.intValue() <= 100) {
                            enchants.put(35, enchants.get(35) + 1);
                        } else {
                            MessageFormatter.sendErrorMessage(player, "You have reached the max level for this enchant.");
                            return;
                        }
                    }
                    prisoner.pickData.enchantments = enchants;
                    save(prisoner);
                    prisoner.update();
                    MessageFormatter.sendSuccessMessage(player, "Your pick's fortune level has increased.");
                    updateDisplay();
                } else {
                    MessageFormatter.sendErrorMessage(player, "You do not have any pick upgrades available.");
                }
            }
        };
        fortune.setDescriptions(Arrays.asList(new String[] { ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Add a level of fortune.", ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Cost: 1", ChatColor.DARK_PURPLE + "Max Level: 100"  }));

        MenuItem haste = new MenuItem(ChatColor.WHITE.toString() + ChatColor.ITALIC + "Haste", new MaterialData(Material.DIAMOND_PICKAXE)) {
            @Override
            public void onClick(Player player) {
                if (prisoner.pickData.availableUpgrades() >= 5) {
                    Map<Integer, Integer> enchants = prisoner.pickData.enchantments;
                    Integer level;
                    if ((level = enchants.get(3)) == null) {
                        enchants.put(3, 1);
                    } else {
                        if (level.intValue() < 4) {
                            enchants.put(3, enchants.get(3) + 1);
                        } else {
                            MessageFormatter.sendErrorMessage(player, "You have reached the max level for this enchant.");
                            return;
                        }
                    }
                    prisoner.pickData.enchantments = enchants;
                    save(prisoner);
                    prisoner.update();
                    MessageFormatter.sendSuccessMessage(player, "Your pick's haste level has increased.");
                    updateDisplay();
                } else {
                    MessageFormatter.sendErrorMessage(player, "You do not have any pick upgrades available.");
                }
            }
        };
        haste.setDescriptions(Arrays.asList(new String[] { ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Add a level of haste.", ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Cost: 5", ChatColor.DARK_PURPLE + "Max Level: 4"  }));

        MenuItem silk = new MenuItem(ChatColor.WHITE.toString() + ChatColor.ITALIC + "Silk Touch", new MaterialData(Material.STRING)) {
            @Override
            public void onClick(Player player) {
                Map<Integer, Integer> enchants = prisoner.pickData.enchantments;
                if (enchants.get(33) == null) {
                    if (prisoner.pickData.availableUpgrades() >= 8) {
                        enchants.put(33, 1);
                        prisoner.pickData.enchantments = enchants;
                        MessageFormatter.sendSuccessMessage(player, "You have unlocked Silk Touch.");
                        updateDisplay();
                    } else {
                        MessageFormatter.sendErrorMessage(player, "You do not have any pick upgrades available.");
                        return;
                    }
                }

                boolean toggle = !prisoner.pickData.silkEnabled;
                prisoner.pickData.silkEnabled = toggle;
                save(prisoner);
                prisoner.update();
            }
        };
        silk.setDescriptions(Arrays.asList(new String[] { ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Adds silk touch to your pick.", ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Cost: 8", ChatColor.DARK_PURPLE + "Toggleable"  }));

        MenuItem vision = new MenuItem(ChatColor.WHITE.toString() + ChatColor.ITALIC + "Night Vision", new MaterialData(Material.EYE_OF_ENDER)) {
            @Override
            public void onClick(Player player) {
                Map<Integer, Integer> enchants = prisoner.pickData.enchantments;
                if (enchants.get(16) == null) {
                    if (prisoner.pickData.availableUpgrades() >= 8) {
                        enchants.put(16, 1);
                        prisoner.pickData.enchantments = enchants;
                        MessageFormatter.sendSuccessMessage(player, "You have unlocked Night Vision.");
                        updateDisplay();
                    } else {
                        MessageFormatter.sendErrorMessage(player, "You do not have any pick upgrades available.");
                        return;
                    }
                }

                boolean toggle = !prisoner.pickData.visionEnabled;
                prisoner.pickData.visionEnabled = toggle;
                save(prisoner);
                prisoner.update();
            }
        };
        vision.setDescriptions(Arrays.asList(new String[] { ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Adds night vision to your pick.", ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Cost: 8", ChatColor.DARK_PURPLE + "Toggleable"  }));

        MenuItem speed = new MenuItem(ChatColor.WHITE.toString() + ChatColor.ITALIC + "Speed Boost", new MaterialData(Material.DIAMOND_BOOTS)) {
            @Override
            public void onClick(Player player) {
                Map<Integer, Integer> enchants = prisoner.pickData.enchantments;
                if (enchants.get(1) == null) {
                    if (prisoner.pickData.availableUpgrades() >= 8) {
                        enchants.put(1, 1);
                        prisoner.pickData.enchantments = enchants;
                        MessageFormatter.sendSuccessMessage(player, "You have unlocked Speed Boost.");
                        updateDisplay();
                    } else {
                        MessageFormatter.sendErrorMessage(player, "You do not have any pick upgrades available.");
                        return;
                    }
                }

                boolean toggle = !prisoner.pickData.speedEnabled;
                prisoner.pickData.speedEnabled = toggle;
                save(prisoner);
                prisoner.update();
            }
        };
        speed.setDescriptions(Arrays.asList(new String[] { ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Adds speed boost to your pick.", ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Cost: 8", ChatColor.DARK_PURPLE + "Toggleable"  }));

        // efficiency
        addMenuItem(efficiency, 0);
        // fortune
        addMenuItem(fortune, 1);
        // haste
        addMenuItem(haste, 2);
        // silk touch
        addMenuItem(silk, 5);
        // night vision
        addMenuItem(vision, 6);
        // speed boost
        addMenuItem(speed, 7);

        MenuItem mines = new MenuItem(ChatColor.BLUE + "Mines", new MaterialData(Material.IRON_PICKAXE)) {
            @Override
            public void onClick(Player player) {
                MineMenu warpMenu = new MineMenu();
                warpMenu.setPlayer(player);
                warpMenu.openMenu(player);
            }
        };
        addMenuItem(mines, 9);

        MenuItem cells = new MenuItem(ChatColor.RED + "Cells", new MaterialData(Material.IRON_FENCE)) {
            @Override
            public void onClick(Player player) {
                CellMenu cellMenu = new CellMenu();
                cellMenu.setPlayer(player);
                cellMenu.openMenu(player);
            }
        };
        addMenuItem(cells, 10);

        MenuItem shop = new MenuItem(ChatColor.DARK_PURPLE + "Shop", new MaterialData(Material.GOLD_NUGGET)) {
            @Override
            public void onClick(Player player) {
                NyxUser user = NyxCore.getUser(player.getUniqueId());
                if (user.getRank().ordinal() < Rank.HERO.ordinal()) {
                    if (NyxPrison.getInstance().getData().shopSpawn != null) {
                        player.teleport(NyxPrison.getInstance().getData().shopSpawn.getLocation());
                        MessageFormatter.sendSuccessMessage(player, "You have arrived at the shops.");
                    } else {
                        MessageFormatter.sendErrorMessage(player, "Only Hero and above can shop remotely.");
                    }
                    return;
                }
                MerchantMenu merchantMenu = new MerchantMenu();
                merchantMenu.setPlayer(player);
                merchantMenu.openMenu(player);
            }
        };
        addMenuItem(shop, 11);

        MenuItem warps = new MenuItem(ChatColor.DARK_PURPLE + "Warps", new MaterialData(Material.COMPASS)) {
            @Override
            public void onClick(Player player) {
                if (WarpMenu.getWarpMenu() == null) {
                    new WarpMenu();
                }
                WarpMenu.getWarpMenu().openMenu(player);
            }
        };
        addMenuItem(warps, 12);

        MenuItem settings = new MenuItem(ChatColor.DARK_PURPLE + "Prisoner Settings", new MaterialData(Material.REDSTONE_COMPARATOR)) {
            @Override
            public void onClick(Player player) {
                PrisonerSettingsMenu.getUserSettings(player).openMenu(player);
            }
        };
        addMenuItem(settings, 17);
    }

    public void save(PrisonerSessionData prisoner) {
        PickDataAPI.savePickEnchants(prisoner);
    }

    public void updateDisplay() {
        removeMenuItem(8);
        MenuItem upgrades = new MenuItem(ChatColor.GREEN.toString() + ChatColor.BOLD + "Pick Upgrades: " + ChatColor.GOLD.toString() + ChatColor.BOLD + prisoner.pickData.availableUpgrades(), new MaterialData(Material.DOUBLE_PLANT)) {
            @Override
            public void onClick(Player player) {
            }
        };
        addMenuItem(upgrades, 8);
    }
}

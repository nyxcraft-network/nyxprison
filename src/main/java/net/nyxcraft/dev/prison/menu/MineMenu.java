package net.nyxcraft.dev.prison.menu;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import net.nyxcraft.dev.prison.command.PrisonCommands;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.entities.Mine;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class MineMenu extends Menu {

    private Player player;

    public MineMenu() {
        super(ChatColor.YELLOW.toString() + ChatColor.ITALIC + "Mine Warp Menu", 6);
    }

    public void setPlayer(Player player) {
        this.player = player;
        init();
    }

    public void init() {
        PrisonerData prisoner = PrisonDatabaseManager.getPrisonerData(player.getUniqueId());
        MenuItem right = new MenuItem(ChatColor.RED + "Recommended Warp -->", new MaterialData(Material.GOLD_INGOT)) {
            @Override
            public void onClick(Player player) {
                return;
            }
        };
        MenuItem left = new MenuItem(ChatColor.RED + "<-- Recommended Warp", new MaterialData(Material.GOLD_INGOT)) {
            @Override
            public void onClick(Player player) {
                return;
            }
        };
        addMenuItem(right, 0, 1);
        addMenuItem(right, 1, 1);
        addMenuItem(left, 7, 1);
        addMenuItem(left, 8, 1);

        for (int i = 0; i <= prisoner.currentRank; i++) {
            String letter = getCharForNumber(i + 1);
            @SuppressWarnings("deprecation")
            MenuItem tba = new MenuItem(ChatColor.DARK_GRAY + "<" + ChatColor.GOLD + letter + ChatColor.DARK_GRAY + ">", new MaterialData(Material.getMaterial(blocks[i]))) {

                @Override
                public void onClick(Player player) {
                    try {
                        Mine mine = PrisonDatabaseManager.getMine(letter);
                        player.teleport(PrisonCommands.stringToLocation(mine.warpLocation));
                        MessageFormatter.sendSuccessMessage(player, "You have teleported to " + mine.casedName + " mine.");
                    } catch (NullPointerException ex) {
                        MessageFormatter.sendErrorMessage(player, "The warp is not set for that mine. Contact an admin.");
                    }
                }
            };
            addMenuItem(tba, 27 + i);
            if (i == prisoner.currentRank) {
                addMenuItem(tba, 4, 1);
            }
        }
    }

    private static String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char) (i + 64)) : null;
    }

    private static int[] blocks = new int[] { 1, 3, 4, 5, 13, 16, 15, 14, 21, 22, 35, 41, 42, 46, 47, 48, 49, 56, 57, 58, 73, 79, 87, 89, 95, 112 };

}

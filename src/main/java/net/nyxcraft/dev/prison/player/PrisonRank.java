package net.nyxcraft.dev.prison.player;

import org.bukkit.ChatColor;

public enum PrisonRank {

    A("A", ChatColor.GRAY, -1, 0, 1),
    B("B", ChatColor.GRAY,                 250000.0, 1, 1.2), 
    C("C", ChatColor.GRAY,                 437500.0, 2, 1.44),
    D("D", ChatColor.GOLD,                 766000.0, 3, 1.72),
    E("E", ChatColor.GOLD,                1340000.0, 4, 2.07),
    F("F", ChatColor.GOLD,                2345000.0, 5, 2.48),
    G("G", ChatColor.DARK_AQUA,           4103000.0, 6, 2.98),
    H("H", ChatColor.DARK_AQUA,           7180000.0, 7, 3.58),
    I("I", ChatColor.DARK_AQUA,          12566000.0, 8, 4.29),
    J("J", ChatColor.DARK_PURPLE,        21990000.0, 9, 5.15),
    K("K", ChatColor.DARK_PURPLE,        38484000.0, 10, 6.19),
    L("L", ChatColor.DARK_PURPLE,        67347000.0, 11, 7.43),
    M("M", ChatColor.BLUE,              117857000.0, 12, 8.91),
    N("N", ChatColor.BLUE,              206225000.0, 13, 10.69),
    O("O", ChatColor.BLUE,              360939000.0, 14, 12.83),
    P("P", ChatColor.GREEN,             631644000.0, 15, 15.40),
    Q("Q", ChatColor.GREEN,            1105377000.0, 16, 18.48),
    R("R", ChatColor.GREEN,            1934411000.0, 17, 22.16),
    S("S", ChatColor.AQUA,             3385219000.0, 18, 26.54),
    T("T", ChatColor.AQUA,             5924134000.0, 19, 31.73),
    U("U", ChatColor.AQUA,            10367234000.0, 20, 37.85),
    V("V", ChatColor.LIGHT_PURPLE,    18142660000.0, 21, 45.03),
    W("W", ChatColor.LIGHT_PURPLE,    31749656000.0, 22, 53.41),
    X("X", ChatColor.LIGHT_PURPLE,    55561898000.0, 23, 63.14),
    Y("Y", ChatColor.DARK_RED,        97233323000.0, 24, 74.38),
    Z("Z", ChatColor.DARK_RED,       170158315000.0, 25, 87.3);

    public int place;
    public String name;
    public ChatColor color;
    public double price;
    public double multiplier;

    private PrisonRank(String name, ChatColor color, double price, int place, double multiplier) {
        this.color = color;
        this.place = place;
        this.name = name;
        this.price = price;
        this.multiplier = multiplier;
    }

    public static PrisonRank nextRank(PrisonRank rank) {
        if (rank.ordinal() + 1 < PrisonRank.values().length) {
            return PrisonRank.values()[rank.ordinal() + 1];
        }

        return null;
    }

}

package net.nyxcraft.dev.prison.player;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import net.nyxcraft.dev.prison.command.RanksCommands;
import net.nyxcraft.dev.prison.database.entities.Cell;
import net.nyxcraft.dev.prison.database.entities.PickData;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;

import net.nyxcraft.dev.prison.database.entities.PrisonerSettings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class PrisonerSessionData {

    private ArrayList<String> baddies = new ArrayList<String>();
    public UUID uuid;
    public int prestige;
    public PrisonRank rank;
    public String gang;
    public double balance;
    public PickData pickData;
    public List<Cell> cells;
    public PrisonerSettings prisonerSettings;
    private Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    private Objective obj = scoreboard.registerNewObjective("nyx_sidebar", "dummy");
    public boolean guard;
    public boolean guardEnabled;

    public PrisonerSessionData(UUID uuid, PrisonerData data) {
        this.uuid = uuid;
        this.prestige = data.prestige;
        this.rank = PrisonRank.values()[data.currentRank];
        this.gang = data.gang;
        this.balance = data.balance;
        this.pickData = data.pickData;
        this.cells = data.cells;
        this.prisonerSettings = data.prisonerSettings;
        this.guard = data.guard;
        this.guardEnabled = data.guardEnabled;

        update();
    }

    public void updateScoreboard() {
        for (String bad : baddies) {
            scoreboard.resetScores(bad);
        }
        baddies.clear();
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(ChatColor.GOLD + "Nyxcraft Prison!");

        obj.getScore(" ").setScore(8);
        obj.getScore(ChatColor.DARK_PURPLE + "Current Rank").setScore(7);
        String rankString = ChatColor.GRAY + rank.name;
        obj.getScore(rankString).setScore(6);
        baddies.add(rankString);

        obj.getScore("  ").setScore(5);
        obj.getScore(ChatColor.DARK_PURPLE + "Balance").setScore(4);
        String balanceString = ChatColor.GRAY + "$" + RanksCommands.formatNumber(this.balance);
        obj.getScore(balanceString).setScore(3);
        baddies.add(balanceString);

        obj.getScore("   ").setScore(2);
        obj.getScore(ChatColor.DARK_PURPLE + "Next Rank").setScore(1);
        String nextRankString = "";
        boolean stop = false;
        PrisonRank nextPrisonRank;
        try {
            nextPrisonRank = PrisonRank.values()[rank.ordinal() + 1];
        } catch (ArrayIndexOutOfBoundsException e) {
            nextPrisonRank = null;
        }

        if (nextPrisonRank != null) {
            double percent = (this.balance / nextPrisonRank.price) * 100.0;
            for (int i = 0; i < 5; i++) {
                if ((percent / 20.0) <= i) {
                    stop = true;
                }
                if (stop) {
                    nextRankString += ChatColor.GRAY + "▌";
                    continue;
                } else {
                    nextRankString += ChatColor.GOLD + "▌";
                }
            }
            if (percent >= 100.0) {
                nextRankString += ChatColor.GRAY + " Done!";
            } else {
                nextRankString += ChatColor.GRAY + "  " + ((int) Math.floor(percent)) + "%";
            }
        } else {
            nextRankString += ChatColor.GRAY + "/prestige";
        }

        obj.getScore(nextRankString).setScore(0);
        baddies.add(nextRankString);

        Bukkit.getPlayer(uuid).setScoreboard(scoreboard);
    }

    public void updatePickMeta(boolean cursorSwap) {
        Player p = Bukkit.getPlayer(uuid);
        ItemMeta meta = p.getInventory().getItem(0).getItemMeta();
        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&5N&8C&e: &7Prison Pickaxe &e&n[Level " + pickData.level + "]&n&5 Exp: " + NumberFormat.getNumberInstance(Locale.US).format(pickData.exp) + "/" + pickData.level * 600));
        List<String> lore = new ArrayList<String>();
        for (Integer perk : pickData.enchantments.keySet()) {
            switch (perk.intValue()) {
                case 1:
                    if (pickData.speedEnabled && !cursorSwap) {
                        lore.add(ChatColor.GRAY + "Speed " + pickData.enchantments.get(perk));
                    }
                    continue;
                case 3:
                    lore.add(ChatColor.GRAY + "Haste " + pickData.enchantments.get(perk));
                    continue;
                case 16:
                    if (pickData.visionEnabled && !cursorSwap) {
                        lore.add(ChatColor.GRAY + "Night Vision " + pickData.enchantments.get(perk));
                    }
                    continue;
                default:
                    continue;
            }
        }
        lore.add("");
        lore.add(ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Exp: " + RanksCommands.formatNumber(pickData.exp) + "/" + pickData.level * 600);
        lore.add(ChatColor.DARK_PURPLE.toString() + ChatColor.ITALIC + "Blocks Mined: " + RanksCommands.formatNumber(pickData.blocksMined));
        meta.setLore(lore);
        p.getInventory().getItem(0).setItemMeta(meta);
    }

    @SuppressWarnings("deprecation")
    public void updatePickEnchantments(boolean cursorSwap) {
        Player p = Bukkit.getPlayer(uuid);
        ItemStack is = p.getInventory().getItem(0);
        if (is == null) {
            p.getInventory().setItem(0, new ItemStack(Material.DIAMOND_PICKAXE));
            is = p.getInventory().getItem(0);
        }
        for (Enchantment enchantment : Enchantment.values()) {
            is.removeEnchantment(enchantment);
        }
        is.getItemMeta().spigot().setUnbreakable(true);
        for (Integer num : pickData.enchantments.keySet()) {
            switch (num.intValue()) {
                case 1:
                    if (pickData.speedEnabled && !cursorSwap) {
                        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, pickData.enchantments.get(1), false, false), true);
                    } else {
                        p.removePotionEffect(PotionEffectType.SPEED);
                    }
                    continue;
                case 3:
                    p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, pickData.enchantments.get(3), false, false), true);
                    continue;
                case 16:
                    if (pickData.visionEnabled && !cursorSwap) {
                        p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, pickData.enchantments.get(16), false, false), true);
                    } else {
                        p.removePotionEffect(PotionEffectType.NIGHT_VISION);
                    }
                    continue;
                case 32:
                    is.addUnsafeEnchantment(Enchantment.getById(num), pickData.enchantments.get(num).intValue());
                    continue;
                case 33:
                    if (pickData.silkEnabled && !cursorSwap) {
                        is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, pickData.enchantments.get(num).intValue());
                    }
                    continue;
                case 35:
                    is.addUnsafeEnchantment(Enchantment.getById(num), pickData.enchantments.get(num).intValue());
                    continue;
                default:
                    continue;
            }
        }
        // NBTTagCompound tag = CraftItemStack.asNMSCopy(is).getTag();
        // tag.
        updatePickMeta(cursorSwap);
    }

    public void update() {
        updatePickEnchantments(false);
        updateScoreboard();
    }

}

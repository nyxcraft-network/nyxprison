package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.entities.Gang;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PVPListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onAttack(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player damager = (Player) event.getDamager();
            if (event.getEntity() instanceof Player) {
                Player target = (Player) event.getEntity();
                if (damager == null || target == null) {
                    return;
                }

                if (target.getWorld().getName().equalsIgnoreCase("cellworld") || inSpawn(damager) || inSpawn(target)) {
                    MessageFormatter.sendErrorMessage(damager, "You cannot attack prisoners in this area.");
                    event.setCancelled(true);
                }

                PrisonerSessionData prisoner1 = NyxPrison.getInstance().getPrisonerSessionData(damager.getUniqueId());
                PrisonerSessionData prisoner2 = NyxPrison.getInstance().getPrisonerSessionData(target.getUniqueId());

                if (prisoner1.prisonerSettings.isPvpEnabled() == false) {
                    MessageFormatter.sendErrorMessage(damager, "You currently have PvP disabled.");
                    event.setCancelled(true);
                    return;
                }

                if (prisoner2.prisonerSettings.isPvpEnabled() == false) {
                    MessageFormatter.sendErrorMessage(damager, target.getName() + " currently has PvP disabled.");
                    event.setCancelled(true);
                    return;
                }

                Gang gang1 = PrisonDatabaseManager.getGang(prisoner1.gang);
                Gang gang2 = PrisonDatabaseManager.getGang(prisoner2.gang);

                if (gang1 == null || gang2 == null) {
                    return;
                }

                if (gang1.name.equals(gang2.name) && !gang1.gangSettings.friendlyFire) {
                    MessageFormatter.sendErrorMessage(damager, "Gang friendly fire is off.");
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        event.setKeepInventory(true);
        event.setKeepLevel(true);
        event.setDeathMessage(null);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        event.setRespawnLocation(NyxPrison.getInstance().getData().getSpawn());
        MessageFormatter.sendInfoMessage(event.getPlayer(), "We can rebuild him. We have the technology. We can make him better than he was. Better, stronger, faster.");
    }

    public boolean inSpawn(Player player) {
        Location spawn = NyxPrison.getInstance().getData().getSpawn();
        Location pos = player.getLocation();

        if (spawn == null || pos == null) {
            return false;
        }

        if (spawn.getWorld() != pos.getWorld()) {
            return false;
        }

        if (spawn.distance(pos) < 15) {
            return true;
        }

        return false;
    }

}

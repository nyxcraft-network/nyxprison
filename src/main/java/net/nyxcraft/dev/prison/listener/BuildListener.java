package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.api.PickDataAPI;
import net.nyxcraft.dev.prison.database.entities.Mine;
import net.nyxcraft.dev.prison.economy.PrisonBlock;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;
import net.nyxcraft.dev.prison.utils.DropUtils;
import net.nyxcraft.dev.prison.world.MineSessionData;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class BuildListener implements Listener {

    public HashMap<UUID, Long> fullInventoryCooldown = new HashMap<>();
    public HashMap<UUID, Long> cantBreakCooldown = new HashMap<>();

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.LOWEST)
    public void onBreak(BlockBreakEvent event) {
        if (event.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("cellworld")) {
            return;
        }

        Location loc = event.getBlock().getLocation();
        Player player = event.getPlayer();
        NyxUser user = NyxCore.getUser(player.getUniqueId());
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());

        if (user.getRank().ordinal() >= Rank.ADMINISTRATOR.ordinal()) {
            if (player.getGameMode() == GameMode.CREATIVE && player.isOp()) {
                return;
            }
        }

        if (event.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("world")) {
            event.setCancelled(true);
            return;
        }

        Mine mine = null;
        for (MineSessionData session : NyxPrison.getInstance().getMines()) {
            if (session.getMine().isIn(loc)) {
                mine = session.getMine();
            }
        }

        if (mine != null) {
            if (prisoner.rank.ordinal() < mine.getRequiredRank().ordinal()) {
                boolean message = true;
                if (cantBreakCooldown.containsKey(player.getUniqueId())) {
                    long secondsLeft = ((cantBreakCooldown.get(player.getUniqueId()) / 1000) + 3) - (System.currentTimeMillis() / 1000);
                    if (secondsLeft > 0) {
                        message = false;
                    }
                }
                if (message) {
                    cantBreakCooldown.put(player.getUniqueId(), System.currentTimeMillis());
                    MessageFormatter.sendErrorMessage(player, "You can't break here");
                }
                event.setCancelled(true);
                return;
            }

            event.setCancelled(true);
            ArrayList<ItemStack> drops = DropUtils.getDrops(event.getBlock(), player.getItemInHand());
            boolean full = true;
            for (ItemStack content : player.getInventory().getContents()) {
                if (!full) {
                    continue;
                }
                if (content == null) {
                    full = false;
                    continue;
                }
                for (ItemStack drop : drops) {
                    if (drop.getType() == content.getType()) {
                        if (content.getAmount() < 64) {
                            full = false;
                        }
                    }
                }
            }

            if (full) {
                boolean message = true;
                if (fullInventoryCooldown.containsKey(player.getUniqueId())) {
                    long secondsLeft = ((fullInventoryCooldown.get(player.getUniqueId()) / 1000) + 3) - (System.currentTimeMillis() / 1000);
                    if (secondsLeft > 0) {
                        message = false;
                    }
                }
                if (message) {
                    fullInventoryCooldown.put(player.getUniqueId(), System.currentTimeMillis());
                    MessageFormatter.sendErrorMessage(player, "Inventory is full");
                }
            } else {
                for (ItemStack drop : drops) {
                    player.getInventory().addItem(drop);
                }
            }

            prisoner.pickData.blocksMined += 1;
            try {
                prisoner.pickData.exp += PrisonBlock.getBlock(event.getBlock().getTypeId(), event.getBlock().getData()).exp;
                int amount = prisoner.pickData.level * 600;
                if (amount <= prisoner.pickData.exp) {
                    MessageFormatter.sendGeneralMessage(event.getPlayer(), "Your pickaxe has leveled up! You now received one Pick Upgrade!");
                    prisoner.pickData.upgrades += 1;
                    prisoner.pickData.exp = prisoner.pickData.exp - amount;
                    prisoner.pickData.level += 1;
                    PickDataAPI.savePickData(prisoner);
                }
            } catch (NullPointerException ex) {

            }

            event.getBlock().setType(Material.AIR);
            prisoner.updatePickMeta(false);
            NyxPrison.getInstance().getMine(mine.name).addBlock();
            return;
        }

        event.setCancelled(true);
    }

    @EventHandler
    public void onLeafDecay(LeavesDecayEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if (e.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("cellworld")) {
            return;
        }

        NyxUser user = NyxCore.getUser(e.getPlayer().getUniqueId());
        if (user.getRank().ordinal() >= Rank.ADMINISTRATOR.ordinal()) {
            if (e.getPlayer().getGameMode() == GameMode.CREATIVE && e.getPlayer().isOp()) {
                return;
            }
        }

        e.setCancelled(true);
    }

}

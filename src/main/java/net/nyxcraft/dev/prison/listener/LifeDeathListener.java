package net.nyxcraft.dev.prison.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.economy.GuardEntity;
import net.nyxcraft.dev.prison.economy.MerchantEntity;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftVillager;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftWitch;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

public class LifeDeathListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        List<ItemStack> tbr = new ArrayList<ItemStack>();
        for (ItemStack is : e.getDrops()) {
            if (is.getType() == Material.DIAMOND_PICKAXE) {
                tbr.add(is);
            }
        }
        e.getDrops().removeAll(tbr);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), new Runnable() {
            @Override
            public void run() {
                PrisonerSessionData psd = NyxPrison.getInstance().getPrisonerSessionData(e.getPlayer().getUniqueId());
                psd.updatePickEnchantments(false);
            }
        }, 1);
    }

    private EntityType[] types = new EntityType[] { EntityType.BAT, EntityType.BLAZE, EntityType.CAVE_SPIDER, EntityType.CHICKEN, EntityType.COW, EntityType.CREEPER, EntityType.ENDERMAN, EntityType.GHAST, EntityType.ZOMBIE, EntityType.WOLF,
            EntityType.WITHER, EntityType.WITCH, EntityType.VILLAGER, EntityType.SPIDER, EntityType.SQUID, EntityType.SNOWMAN, EntityType.IRON_GOLEM, EntityType.MAGMA_CUBE, EntityType.MUSHROOM_COW, EntityType.OCELOT, EntityType.PIG,
            EntityType.PIG_ZOMBIE, EntityType.RABBIT, EntityType.SHEEP, EntityType.SILVERFISH, EntityType.SKELETON, EntityType.SLIME };

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent e) {
        if (e.getEntityType() == EntityType.VILLAGER) {
            if (((CraftVillager) e.getEntity()).getHandle() instanceof GuardEntity) {
                return;
            }
        }
        if (e.getEntityType() == EntityType.WITCH) {
            if (((CraftWitch) e.getEntity()).getHandle() instanceof MerchantEntity) {
                return;
            }
        }
        if (Arrays.asList(types).contains(e.getEntityType())) {
            e.getEntity().remove();
        }
    }
}

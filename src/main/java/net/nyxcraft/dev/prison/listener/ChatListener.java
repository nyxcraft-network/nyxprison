package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.event.AnnouncementsUpdatedEvent;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.command.GangCommands;
import net.nyxcraft.dev.prison.database.entities.Gang;
import net.nyxcraft.dev.prison.gang.GangManager;
import net.nyxcraft.dev.prison.player.PrisonRank;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.UUID;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        Rank rank = NyxCore.getUser(e.getPlayer().getUniqueId()).getRank();
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(e.getPlayer().getUniqueId());
        if (GangCommands.getChatters().contains(e.getPlayer().getName())) {
            if (prisoner.gang != null) {
                Gang gang = GangManager.get(prisoner.gang);
                if (gang == null) {
                    return;
                }

                String message = ChatColor.DARK_GRAY + "[" + ChatColor.DARK_PURPLE + gang.casedName + ChatColor.DARK_GRAY + "] " + ChatColor.RESET + e.getPlayer().getName() + ": " + ChatColor.BLUE + e.getMessage();
                for (String string : gang.members) {
                    Player member = Bukkit.getPlayer(UUID.fromString(string));
                    if (member == null) {
                        continue;
                    } else {
                        member.sendMessage(message);
                    }
                }

                Player leader = Bukkit.getPlayer(UUID.fromString(gang.leader));
                if (leader != null) {
                    leader.sendMessage(message);
                }

                e.setCancelled(true);
                return;
            }
        }

        String format;
        PrisonRank prisonRank = prisoner.rank;
        if (!prisoner.guardEnabled) {
            format = ChatColor.DARK_GRAY + "[" + prisonRank.color + (prisoner.prestige == 0 ? "" : prisoner.prestige) + prisonRank.name + ChatColor.DARK_GRAY + "]";
        } else {
            format = ChatColor.DARK_GRAY + "[" + ChatColor.BLUE + "Guard" + ChatColor.DARK_GRAY + "]";
        }

        if (rank != Rank.DEFAULT) {
            format += " [" + rank.color + rank.prefix + ChatColor.DARK_GRAY + "]";
        }

        format += (rank.ordinal() >= Rank.HELPER.ordinal() ? ChatColor.WHITE : ChatColor.GRAY) + " %s: %s";
        e.setFormat(format);
    }

    @EventHandler
    public void onAnnouncementsUpdated(AnnouncementsUpdatedEvent event) {
        NyxPrison.getInstance().getActionAnnouncer().setMessages(event.getNetworkSettings().announcements.get("prison"));
    }
}

package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.menu.WarpMenu.WarpData;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {

    @EventHandler
    public void onSendCommand(PlayerCommandPreprocessEvent e) {
        WarpData warpData = null;
        String target = null;
        for (String warp : NyxPrison.getInstance().warps.keySet()) {
            if (e.getMessage().split(" ")[0].replaceAll("/", "").equals(warp)) {
                warpData = NyxPrison.getInstance().warps.get(warp);
                target = warp;
            }
        }
        if (warpData != null) {
            e.getPlayer().teleport(warpData.loc);
            MessageFormatter.sendSuccessMessage(e.getPlayer(), "Warped to " + target);
            e.setCancelled(true);
        } else {
            return;
        }
    }

}

package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.entities.Cell;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Chunk;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class CellListener implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        NyxUser user = NyxCore.getUser(e.getPlayer().getUniqueId());
        if (user.getRank().ordinal() >= Rank.ADMINISTRATOR.ordinal()) {
            if (e.getPlayer().getGameMode() == GameMode.CREATIVE && e.getPlayer().isOp()) {
                return;
            }
        }
        if (e.getBlock().getLocation().getWorld().getName().equals("cellworld")) {
            if (!inPlayerCell(e.getPlayer(), e.getBlock().getLocation())) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        NyxUser user = NyxCore.getUser(e.getPlayer().getUniqueId());
        if (user.getRank().ordinal() >= Rank.ADMINISTRATOR.ordinal()) {
            if (e.getPlayer().getGameMode() == GameMode.CREATIVE && e.getPlayer().isOp()) {
                return;
            }
        }
        if (e.getBlock().getLocation().getWorld().getName().equals("cellworld")) {
            if (!inPlayerCell(e.getPlayer(), e.getBlock().getLocation())) {
                e.setCancelled(true);
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        NyxUser user = NyxCore.getUser(e.getPlayer().getUniqueId());
        if (e.getClickedBlock() != null) {
            if (e.getClickedBlock().getLocation().getWorld().getName().equals("cellworld")) {
                Block block = e.getClickedBlock();
                if (block.getType() == Material.IRON_DOOR_BLOCK) {
                    if (block.getData() >= 8) {
                        block = block.getRelative(BlockFace.DOWN);
                    }
                    if (block.getType() == Material.IRON_DOOR_BLOCK) {
                        if (block.getData() < 4) {
                            block.setData((byte) (block.getData() + 4));
                            block.getWorld().playEffect(block.getLocation(), Effect.DOOR_TOGGLE, 0);
                        } else {
                            block.setData((byte) (block.getData() - 4));
                            block.getWorld().playEffect(block.getLocation(), Effect.DOOR_TOGGLE, 0);
                        }
                        e.setUseItemInHand(Event.Result.DENY);
                    }
                }
                if (user.getRank().ordinal() >= Rank.ADMINISTRATOR.ordinal()) {
                    return;
                }
                if (!inPlayerChunk(e.getPlayer(), e.getClickedBlock().getLocation())) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity().getLocation().getWorld().equals("cellworld")) {
            if (e.getEntity().getType() == EntityType.PLAYER) {
                MessageFormatter.sendErrorMessage((Player) e.getEntity(), "Cancelling damage");
                e.setCancelled(true);
            }
        }
    }

    public boolean inPlayerChunk(Player p, Location loc) {
        if (!loc.getWorld().getName().equals("cellworld")) {
            return false;
        }
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(p.getUniqueId());
        for (Cell cs : prisoner.cells) {
            Chunk chunk = loc.getWorld().getChunkAt(cs.chunkX, cs.chunkZ);
            if (loc.getChunk().equals(chunk)) {
                return true;
            }
        }
        return false;
    }

    public boolean inPlayerCell(Player p, Location loc) {
        if (!loc.getWorld().getName().equals("cellworld")) {
            return false;
        }
        PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(p.getUniqueId());
        for (Cell cs : prisoner.cells) {
            Chunk chunk = loc.getWorld().getChunkAt(cs.chunkX, cs.chunkZ);
            if (!loc.getChunk().equals(chunk)) {
                continue;
            }

            double xLowerBound = 2;
            double xUpperBound = 13;
            double zLowerBound = 1;
            double zUpperBound = 14;

            if (loc.getX() < 0) {
                xLowerBound = 3;
                xUpperBound = 14;
            }

            if (loc.getZ() < 0) {
                zLowerBound = 2;
                zUpperBound = 15;
            }

            if (loc.getY() < 62 && loc.getY() > 54) {
                if ((Math.abs(loc.getBlockX()) % 16 >= xLowerBound && Math.abs(loc.getBlockX()) % 16 <= xUpperBound)) {
                    if (Math.abs(loc.getBlockZ()) % 16 >= zLowerBound && Math.abs(loc.getBlockZ()) % 16 <= zUpperBound) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

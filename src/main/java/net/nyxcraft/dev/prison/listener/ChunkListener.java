package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.command.PrisonCommands;
import net.nyxcraft.dev.prison.database.dao.GuardDAO;
import net.nyxcraft.dev.prison.database.dao.MerchantDAO;
import net.nyxcraft.dev.prison.database.entities.Guard;
import net.nyxcraft.dev.prison.database.entities.Merchant;
import net.nyxcraft.dev.prison.economy.GuardEntity;
import net.nyxcraft.dev.prison.economy.MerchantEntity;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

public class ChunkListener implements Listener {

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e) {
        if (e.getWorld().getName().equalsIgnoreCase("cellworld")) {
            return;
        }
        for (Guard guard : GuardDAO.getInstance().createQuery()) {
            Location loc = PrisonCommands.stringToLocation(guard.location);
            if (loc.getChunk() == e.getChunk()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        if (e.getChunk().isLoaded()) {
                            GuardEntity.loadGuard(loc, guard.id);
                        }
                    }
                }, 5);

            }
        }
        for (Merchant merchant : MerchantDAO.getInstance().createQuery()) {
            Location loc = PrisonCommands.stringToLocation(merchant.location);
            if (loc.getChunk() == e.getChunk()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        if (e.getChunk().isLoaded()) {
                            MerchantEntity.loadMerchant(loc, merchant.id);
                        }
                    }
                }, 5);
            }
        }
    }
}

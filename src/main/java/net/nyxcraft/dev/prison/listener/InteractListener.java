package net.nyxcraft.dev.prison.listener;

import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.server.v1_8_R1.EntityVillager;
import net.minecraft.server.v1_8_R1.EntityWitch;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.command.RanksCommands;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.api.PrisonerDataAPI;
import net.nyxcraft.dev.prison.database.dao.GuardDAO;
import net.nyxcraft.dev.prison.database.dao.MerchantDAO;
import net.nyxcraft.dev.prison.economy.GuardEntity;
import net.nyxcraft.dev.prison.economy.MerchantEntity;
import net.nyxcraft.dev.prison.economy.PrisonBlock;
import net.nyxcraft.dev.prison.menu.MerchantMenu;
import net.nyxcraft.dev.prison.menu.PickaxePerkMenu;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class InteractListener implements Listener {

    private static ArrayList<UUID> selling = new ArrayList<>();
    private static ArrayList<UUID> smelting = new ArrayList<>();
    private static ArrayList<ItemStack> smeltingItems = new ArrayList<>();

    @SuppressWarnings({ "deprecation", "incomplete-switch" })
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getPlayer().getItemInHand().getType() == Material.DIAMOND_PICKAXE && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
            PickaxePerkMenu menu = new PickaxePerkMenu();
            menu.setPlayer(e.getPlayer());
            menu.openMenu(e.getPlayer());
        }
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.FURNACE) {
            e.setCancelled(true);
            if (smelting.contains(e.getPlayer().getUniqueId())) {
                MessageFormatter.sendErrorMessage(e.getPlayer(), "You are currently smelting items right now.");
                return;
            }
            @SuppressWarnings("unused")
            double smeltAmmount = 0;
            int count = 0;
            for (ItemStack is : e.getPlayer().getInventory().getContents()) {
                if (is != null) {
                    try {
                        PrisonBlock pb = PrisonBlock.getBlock(is.getTypeId(), is.getData().getData());
                        if (pb.smeltable) {
                            count++;
                            smeltingItems.add(is);
                        }
                    } catch (Exception ex) {
                        //
                    }
                }
            }
            if (count == 0) {
                MessageFormatter.sendInfoMessage(e.getPlayer(), "You don't have anything to smelt!");
                return;
            }

            MessageFormatter.sendInfoMessage(e.getPlayer(), "Your items will be smelted, it will take &e&n" + count + " seconds.");
            smelting.add(e.getPlayer().getUniqueId());

            Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), () -> {
                if (e.getPlayer() != null) {
                    // remove the ores from the Inventory
                    for (ItemStack is : smeltingItems) {
                        e.getPlayer().getInventory().remove(is);
                    }

                    // Replace the Ores with the Ingots
                    ArrayList<ItemStack> tbs_clone = new ArrayList<>();
                    tbs_clone = smeltingItems;
                    for (int i = 0; i < tbs_clone.size(); i++) {
                        ItemStack stack = tbs_clone.get(i);
                        switch (stack.getType()) {
                            case GOLD_ORE:
                                stack.setType(Material.GOLD_INGOT);
                                smeltingItems.set(i, stack);
                                break;
                            case IRON_ORE:
                                stack.setType(Material.IRON_INGOT);
                                smeltingItems.set(i, stack);
                                break;
                            case COBBLESTONE:
                                stack.setType(Material.STONE);
                                smeltingItems.set(i, stack);
                                break;
                        }
                    }

                    // Put the Ingots in the Inventory
                    for (ItemStack is : smeltingItems) {
                        e.getPlayer().getInventory().addItem(is);
                    }

                    MessageFormatter.sendSuccessMessage(e.getPlayer(), "Your Items have been smelted and added to your Inventory");

                    // Remove him from the smelting list
                    smelting.remove(e.getPlayer().getUniqueId());
                    smeltingItems.clear();
                }
            }, count * 20);
        }
        // if (e.getPlayer().getItemInHand().getType() ==
        // Material.DIAMOND_PICKAXE && e.getAction() == Action.LEFT_CLICK_BLOCK)
        // {
        // boolean inMine = false;
        // for (Mine iterate : MineDAO.getInstance().createQuery()) {
        // if (iterate.isIn(e.getClickedBlock().getLocation())) {
        // inMine = true;
        // }
        // }
        //
        // if (e.getClickedBlock().getType() == Material.GLASS && inMine) {
        // e.getClickedBlock().setType(Material.AIR);
        // e.setCancelled(true);
        // }
        // }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        if (e.getRightClicked().getType() == EntityType.VILLAGER) {
            EntityVillager entity = (EntityVillager) ((CraftEntity) e.getRightClicked()).getHandle();
            if (entity instanceof GuardEntity) {
                GuardEntity guard = (GuardEntity) entity;
                e.setCancelled(true);
                if (e.getPlayer().isSneaking()) {
                    NyxUser user = NyxCore.getUser(e.getPlayer().getUniqueId());
                    if (user.getRank().ordinal() >= Rank.ADMINISTRATOR.ordinal()) {
                        guard.die();
                        GuardDAO.getInstance().delete(PrisonDatabaseManager.getGuard(guard.id));
                        return;
                    }
                }

                if (selling.contains(e.getPlayer().getUniqueId())) {
                    MessageFormatter.sendErrorMessage(e.getPlayer(), "You are currently selling items right now.");
                    return;
                }

                if (smelting.contains(e.getPlayer().getUniqueId())) {
                    MessageFormatter.sendErrorMessage(e.getPlayer(), "You are currently smelting items. You cannot sell anything");
                    return;
                }

                double sellAmount = 0;
                int count = 0;
                ArrayList<ItemStack> tbr = new ArrayList<ItemStack>();
                for (ItemStack is : e.getPlayer().getInventory().getContents()) {
                    if (is != null) {
                        try {
                            PrisonBlock pb = PrisonBlock.getBlock(is.getTypeId(), is.getData().getData());
                            sellAmount += pb.price * is.getAmount();
                            count++;
                            tbr.add(is);
                        } catch (Exception ex) {
                            continue;
                        }
                    }
                }

                if (count == 0) {
                    e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&lGUARD&7: &fYou don't have any items to sell me!"));
                    return;
                }

                for (ItemStack is : tbr) {
                    e.getPlayer().getInventory().remove(is);
                }

                PrisonerSessionData prisoner = NyxPrison.getInstance().getPrisonerSessionData(e.getPlayer().getUniqueId());
                e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&lGUARD&7: &fI'm selling your items, it will take &e&n" + count + " seconds."));
                selling.add(e.getPlayer().getUniqueId());
                final double finalAmount = sellAmount * prisoner.rank.multiplier * (1.0 + (.01 * (double) prisoner.prestige));

                Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), () -> {
                    if (e.getPlayer() != null) {
                        selling.remove(e.getPlayer().getUniqueId());
                        prisoner.balance += finalAmount;
                        prisoner.updateScoreboard();
                        PrisonerDataAPI.setBalance(prisoner);
                        e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&lGUARD&7: &fYour items were sold for &e$" + RanksCommands.formatNumber(finalAmount)));
                    }
                }, count * 20);
            }
        } else if (e.getRightClicked().getType() == EntityType.WITCH) {
            EntityWitch entity = (EntityWitch) ((CraftEntity) e.getRightClicked()).getHandle();
            if (entity instanceof MerchantEntity) {
                MerchantEntity merchant = (MerchantEntity) entity;
                e.setCancelled(true);

                if (e.getPlayer().isSneaking()) {
                    NyxUser user = NyxCore.getUser(e.getPlayer().getUniqueId());
                    if (user.getRank().ordinal() >= Rank.ADMINISTRATOR.ordinal()) {
                        merchant.die();
                        MerchantDAO.getInstance().delete(PrisonDatabaseManager.getMerchant(merchant.id));
                        return;
                    }
                }

                MerchantMenu menu = MerchantMenu.getMerchantMenu(e.getPlayer());
                menu.openMenu(e.getPlayer());
            }
        }
    }

    public static ArrayList<UUID> getSelling() {
        return selling;
    }

    public static ArrayList<UUID> getSmelting() {
        return smelting;
    }

    // Needed to cancel Inventory Interaction
    public static ArrayList<ItemStack> getSmeltingItems() {
        return smeltingItems;
    }
}

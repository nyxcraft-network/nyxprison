package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class InventoryListener implements Listener {

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        boolean isSmelting = false;
        if (InteractListener.getSmelting().contains(e.getPlayer().getUniqueId())) {
            isSmelting = true;
        }

        if (isSmelting) {
            if (InteractListener.getSmeltingItems().contains(e.getItemDrop().getItemStack())) {
                e.setCancelled(true);
                Bukkit.getScheduler().scheduleSyncDelayedTask(NyxPrison.getInstance(), () -> {
                    e.getPlayer().updateInventory();
                }, 1L);
            }
        }
        try {
            if (e.getItemDrop().getItemStack().getItemMeta().getDisplayName().contains("Prison Pickaxe")) {
                e.setCancelled(true);
            }
        } catch (NullPointerException ex) {
            return;
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getSlotType() == InventoryType.SlotType.QUICKBAR) {
            if (event.getSlot() == 0) {
                event.setCancelled(true);
            }
        }

        if (event.getAction() == InventoryAction.HOTBAR_SWAP || event.getAction() == InventoryAction.SWAP_WITH_CURSOR) {
            if (event.getHotbarButton() == 0) {
                event.setCancelled(true);
            }
        }

        boolean isSmelting = false;
        if (event.getRawSlot() >= 0 && InteractListener.getSmelting().contains(event.getWhoClicked().getUniqueId())) {
            isSmelting = true;
        }

        if (isSmelting) {
            ArrayList<ItemStack> stacks = InteractListener.getSmeltingItems();
            if (stacks.contains(event.getCurrentItem())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerItemHeld(PlayerItemHeldEvent event) {
        if (event.getPreviousSlot() == 0 || event.getNewSlot() == 0) {
            PrisonerSessionData data = NyxPrison.getInstance().getPrisonerSessionData(event.getPlayer().getUniqueId());
            data.updatePickEnchantments(event.getPreviousSlot() == 0 ? true : false);
        }
    }
}

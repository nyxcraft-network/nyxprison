package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.prison.NyxPrison;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.api.PickDataAPI;
import net.nyxcraft.dev.prison.database.dao.PrisonerDataDAO;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;
import net.nyxcraft.dev.prison.menu.PrisonerSettingsMenu;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        PrisonerData prisonerData = PrisonDatabaseManager.initPrisonerData(e.getPlayer());
        PrisonerSessionData prisonerSessionData = new PrisonerSessionData(e.getPlayer().getUniqueId(), prisonerData);
        NyxPrison.getInstance().addPrisoner(prisonerSessionData);
        e.getPlayer().teleport(NyxPrison.getInstance().getData().getSpawn());
        e.getPlayer().setHealth(20.0);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        PrisonerSettingsMenu.cleanup(e.getPlayer());
        saveUser(e.getPlayer());
    }

    public static void saveUser(Player player) {
        //Remove him from smelting and selling list if they log off / get kicked off while doing these things
        InteractListener.getSelling().remove(player.getUniqueId());
        InteractListener.getSmelting().remove(player.getUniqueId());

        PrisonerData prisonerData = PrisonDatabaseManager.getPrisonerData(player.getUniqueId());
        PrisonerSessionData prisonerSessionData = NyxPrison.getInstance().getPrisonerSessionData(player.getUniqueId());
        prisonerData.currentRank = prisonerSessionData.rank.ordinal();
        prisonerData.prestige = prisonerSessionData.prestige;
        prisonerData.balance = prisonerSessionData.balance;
        prisonerData.prisonerSettings = prisonerSessionData.prisonerSettings;
        PickDataAPI.savePickData(prisonerSessionData.pickData);
        PrisonerDataDAO.getInstance().save(prisonerData);
    }

}
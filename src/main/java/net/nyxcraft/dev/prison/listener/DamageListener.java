package net.nyxcraft.dev.prison.listener;

import net.nyxcraft.dev.nyxutils.reflection.CommonReflection;
import net.nyxcraft.dev.prison.command.PrisonCommands;
import net.nyxcraft.dev.prison.economy.GuardEntity;
import net.nyxcraft.dev.prison.economy.MerchantEntity;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Witch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

public class DamageListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity().getType() == EntityType.PLAYER) {
            if (e.getCause() == DamageCause.VOID) {
                PrisonCommands.spawn((Player) e.getEntity(), new String[]{""});
            } else if (e.getCause() == DamageCause.ENTITY_ATTACK) {
                return;
            }

            e.setCancelled(true);
        } else if (e.getEntity().getType() == EntityType.VILLAGER) {
            if (CommonReflection.getHandle(e.getEntity()) instanceof GuardEntity) {
                e.setCancelled(true);
            }
        } else if (e.getEntity().getType() == EntityType.WITCH) {
            if (CommonReflection.getHandle(e.getEntity()) instanceof MerchantEntity) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent e) {
        if (e.getFoodLevel() < 20) {
            e.setFoodLevel(20);
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Witch) {
            if (CommonReflection.getHandle((Witch) event.getEntity().getShooter()) instanceof MerchantEntity) {
                event.setCancelled(true);
            }
        }
    }

}

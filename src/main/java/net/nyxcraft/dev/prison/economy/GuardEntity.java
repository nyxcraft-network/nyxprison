package net.nyxcraft.dev.prison.economy;

import java.util.ArrayList;

import net.minecraft.server.v1_8_R1.Entity;
import net.minecraft.server.v1_8_R1.EntityVillager;
import net.minecraft.server.v1_8_R1.GenericAttributes;
import net.minecraft.server.v1_8_R1.World;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.entity.Villager.Profession;

public class GuardEntity extends EntityVillager {

    private static ArrayList<GuardEntity> guards = new ArrayList<GuardEntity>();

    public int id;

    public GuardEntity(World world) {
        super(world);
        guards.add(this);
    }

    @Override
    public void g(double x, double y, double z) {
        return;
    }

    @SuppressWarnings("deprecation")
    public static void loadGuard(Location loc, int id) {
        GuardEntity customGuard = new GuardEntity(((CraftWorld) loc.getWorld()).getHandle());
        customGuard.setLocation(loc.getX(), loc.getY(), loc.getY(), loc.getYaw(), loc.getPitch());
        customGuard.setAge(0);
        customGuard.ageLocked = true;
        customGuard.persistent = true;
        customGuard.setCustomName(CustomEntityEnum.GUARD.name);
        customGuard.setCustomNameVisible(true);
        customGuard.setProfession(Profession.BLACKSMITH.getId());
        customGuard.getAttributeInstance(GenericAttributes.d).setValue(0);
        customGuard.id = id;
        spawnEntity(customGuard, loc);
    }

    public static void spawnEntity(Entity entity, Location loc) {
        entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw());
        ((CraftWorld) loc.getWorld()).getHandle().addEntity(entity);
    }

}

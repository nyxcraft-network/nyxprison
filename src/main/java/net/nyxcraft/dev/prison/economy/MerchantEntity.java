package net.nyxcraft.dev.prison.economy;

import java.util.ArrayList;

import net.minecraft.server.v1_8_R1.Entity;
import net.minecraft.server.v1_8_R1.EntityWitch;
import net.minecraft.server.v1_8_R1.GenericAttributes;
import net.minecraft.server.v1_8_R1.World;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;

public class MerchantEntity extends EntityWitch {

    private static ArrayList<MerchantEntity> sellers = new ArrayList<>();

    public int id;

    public MerchantEntity(World world) {
        super(world);
        sellers.add(this);
    }

    @Override
    public void g(double x, double y, double z) {
        return;
    }

    public static void loadMerchant(Location loc, int id) {
        MerchantEntity customMerchant = new MerchantEntity(((CraftWorld) loc.getWorld()).getHandle());
        customMerchant.setLocation(loc.getX(), loc.getY(), loc.getY(), loc.getYaw(), loc.getPitch());
        customMerchant.persistent = true;
        customMerchant.setCustomName(CustomEntityEnum.WITCH_MERCHANT.name);
        customMerchant.setCustomNameVisible(true);
        customMerchant.getAttributeInstance(GenericAttributes.d).setValue(0);
        customMerchant.id = id;
        spawnEntity(customMerchant, loc);

    }

    public static void spawnEntity(Entity entity, Location loc) {
        entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw());
        ((CraftWorld) loc.getWorld()).getHandle().addEntity(entity);
    }

}

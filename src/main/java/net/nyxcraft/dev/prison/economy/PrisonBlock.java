package net.nyxcraft.dev.prison.economy;

public enum PrisonBlock {
    
    ANDESITE(              1,  5,    20,   1,   false),
    BRICKS(               45,  0,    15,   1,   false),
    COAL(                263,  0,    15,  -1,   false),
    COAL_ORE(             16,  0,    20,   2,   false),
    COAL_BLOCK(          173,  0,   130,   2,   false),
    COBBLESTONE(           4,  0,    5,    1,   true),
    DIAMOND(             264,  0,   150,  -1,   false),
    DIAMOND_ORE(          56,  0,   450,   6,   false),
    DIAMOND_BLOCK(        57,  0,  1350,   6,   false),
    DIORITE(               1,  3,    15,   1,   false),
    EMERALD(             388,  0,   135,  -1,   false),
    EMERALD_ORE(         129,  0,   300,   7,   false),
    EMERALD_BLOCK(       133,  0,  1215,  7,  false),
    GOLD_INGOT(          266,  0,   100,   1,   false),
    GOLD_ORE(             14,  0,    70,   4,   true),
    GOLD_BLOCK(           41,  0,   900,   4,   false),
    GRANITE(               1,  1,    30,   1,   false),
    IRON_INGOT(          265,  0,    80,  -1,   false),
    IRON_ORE(             15,  0,    60,   2,   true),
    IRON_BLOCK(           42,  0,   720,   2,   false),
    LAPIS_BLOCK(          22,  0,  1000,   5,   false),
    NETHERRACK(           87,  0,    37,   1,   false),
    PACKED_ICE(          174,  0,    50,   3,   false),
    POLISHED_ANDESITE(     1,  6,    25,   1,   false),
    POLISHED_DIORITE(      1,  4,    25,   1,   false),
    POLISHED_GRANITE(      1,  2,    35,   1,   false),
    PRISMARINE(          168,  0,    40,   2,   false),
    PRISMARINE_BRICKS(   168,  1,    45,   2,   false),
    PRISMARINE_CRYSTALS( 410,  0,   135,  -1,   false),
    QUARTZ(              406,  0,    10,  -1,   false),
    QUARTZ_BLOCK(        155,  0,    40,   1,   false),
    QUARTZ_ORE(          153,  0,    30,   2,   false),
    REDSTONE_BLOCK(      152,  0,   250,   3,   false),
    STONE(                1,   0,    20,   1,   false);

    public int id;
    public int data;
    public double price;
    public double exp;
    public boolean smeltable;
    
    private PrisonBlock(int id, int data, double price, double exp, boolean smeltable) {
        this.id = id;
        this.data = data;
        this.price = price;
        this.exp = exp;
        this.smeltable = smeltable;
    }
    
    public static PrisonBlock getBlock(int id, int data) {
        for(PrisonBlock prisonBlock : PrisonBlock.values()) {
            if(prisonBlock.id == id && prisonBlock.data == data) {
                return prisonBlock;
            }
        }
        return null;
    }

}

package net.nyxcraft.dev.prison.economy;

import java.lang.reflect.Field;
import java.util.Map;

import net.minecraft.server.v1_8_R1.Entity;
import net.minecraft.server.v1_8_R1.EntityTypes;

import org.bukkit.ChatColor;

public enum CustomEntityEnum {

    WITCH_MERCHANT(ChatColor.GOLD + "Merchant", 66, MerchantEntity.class),
    GUARD(ChatColor.GOLD + "Guard", 120, GuardEntity.class);

    public String name;
    
    private CustomEntityEnum(String name, int id, Class<? extends Entity> custom) {
        addToMaps(custom, name, id);
        this.name = name;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void addToMaps(Class clazz, String name, int id) {
        ((Map) getPrivateField("d", EntityTypes.class, null)).put(clazz, name);
        ((Map) getPrivateField("f", EntityTypes.class, null)).put(clazz, Integer.valueOf(id));
    }

    @SuppressWarnings("rawtypes")
    public static Object getPrivateField(String fieldName, Class clazz, Object object) {
        Field field;
        Object o = null;

        try {
            field = clazz.getDeclaredField(fieldName);

            field.setAccessible(true);

            o = field.get(object);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return o;
    }

}

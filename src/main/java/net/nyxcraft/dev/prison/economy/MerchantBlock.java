package net.nyxcraft.dev.prison.economy;

public enum MerchantBlock {

    LOG(17, 0, 150.0, 16),
    SRUCE_LOG(17, 1, 150.0, 16),
    BIRCH_LOG(17, 2, 150.0, 16),
    GLOWSTONE(89, 0, 250.0, 8),
    SEEDS(295, 0, 100.0, 8),
    PUMPKING_SEEDS(361, 0, 150.0, 8),
    MELON_SEEDS(362, 0, 150.0, 8);
    

    public int id;
    public int data;
    public double price;
    public int quantity;

    private MerchantBlock(int id, int data, double price, int quantity) {
        this.id = id;
        this.data = data;
        this.price = price;
        this.quantity = quantity;
    }

}

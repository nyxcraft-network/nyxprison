package net.nyxcraft.dev.prison.utils;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class DropUtils {

    @SuppressWarnings("deprecation")
    public static ArrayList<ItemStack> getDrops(Block block, ItemStack tool) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        if (tool.getEnchantmentLevel(Enchantment.SILK_TOUCH) >= 1) {
            drops.add(new ItemStack(block.getType(), getDropCount(tool)));
        } else {
            for (ItemStack is : block.getDrops()) {
                drops.add(new ItemStack(is.getType(), getDropCount(tool), (short) 0, is.getData().getData()));
            }
        }
        return drops;
    }

    private static int getDropCount(ItemStack is) {
        int i = is.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
        Random r = new Random();
        int j = r.nextInt(i + 2) - 1;
        if (j < 0) {
            j = 0;
        }
        return (j + 1);
    }

}

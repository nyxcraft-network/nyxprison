package net.nyxcraft.dev.prison.database.entities;

import java.util.HashMap;
import java.util.Map;

import net.nyxcraft.dev.nyxcore.world.SerializeableLocation;
import net.nyxcraft.dev.prison.database.api.PrisonDataAPI;

import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "network_prison_data", noClassnameStored = true)
public class PrisonData {

    @Id
    public ObjectId objectId;
    public Map<String, String> warps = new HashMap<String, String>();
    public Map<String, Material> warpItems = new HashMap<String, Material>();
    public int currentCellId;
    public SerializeableLocation spawn = null;
    public SerializeableLocation shopSpawn = null;

    public PrisonData() {
    }

    public Location getSpawn() {
        if (spawn == null) {
            return Bukkit.getWorlds().get(0).getSpawnLocation();
        }

        return spawn.getLocation();
    }

    public void incrementCellId() {
        currentCellId += 1;
        PrisonDataAPI.incrementCurrentCellId();
    }
}

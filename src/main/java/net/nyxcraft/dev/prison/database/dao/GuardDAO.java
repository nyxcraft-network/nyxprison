package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.Guard;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class GuardDAO extends BasicDAO<Guard, ObjectId> {

    public static GuardDAO instance;
    
    public GuardDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }
    
    public static GuardDAO getInstance() {
        return instance;
    }
    
}

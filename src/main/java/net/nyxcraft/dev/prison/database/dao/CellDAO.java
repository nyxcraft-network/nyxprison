package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.Cell;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class CellDAO extends BasicDAO<Cell, ObjectId> {

    private static CellDAO instance;

    public CellDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static CellDAO getInstance() {
        return instance;
    }

}

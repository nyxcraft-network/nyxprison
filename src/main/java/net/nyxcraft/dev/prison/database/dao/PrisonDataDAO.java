package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.PrisonData;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class PrisonDataDAO extends BasicDAO<PrisonData, ObjectId> {

    private static PrisonDataDAO instance;

    public PrisonDataDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static PrisonDataDAO getInstance() {
        return instance;
    }
}

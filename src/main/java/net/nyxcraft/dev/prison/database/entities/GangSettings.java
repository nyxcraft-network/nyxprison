package net.nyxcraft.dev.prison.database.entities;

public class GangSettings {

    public boolean friendlyFire = false;
    public boolean inviteOnly = true;
    public boolean freeInvite = false;

}

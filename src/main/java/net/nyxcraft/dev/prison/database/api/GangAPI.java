package net.nyxcraft.dev.prison.database.api;

import net.nyxcraft.dev.prison.database.dao.GangDAO;
import net.nyxcraft.dev.prison.database.entities.Gang;
import net.nyxcraft.dev.prison.gang.GangManager;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

import java.util.UUID;

public class GangAPI {

    public static void create(Gang gang) {
        GangDAO.getInstance().save(gang);
        GangManager.store(gang);
    }

    public static void disband(String gang) {
        GangDAO.getInstance().deleteByQuery(GangDAO.getInstance().createQuery().field("name").equal(gang));
        GangManager.remove(gang);
    }

    public static void addMember(UUID uuid, Gang gang) {
        UpdateOperations<Gang> ops = GangDAO.getInstance().createUpdateOperations();
        ops.add("members", uuid.toString());
        update(gang.name, ops);
    }

    public static void removeMember(UUID uuid, Gang gang) {
        UpdateOperations<Gang> ops = GangDAO.getInstance().createUpdateOperations();
        ops.removeAll("members", uuid.toString());
        update(gang.name, ops);
    }

    public static void updateSettings(Gang gang) {
        UpdateOperations<Gang> ops = GangDAO.getInstance().createUpdateOperations();
        ops.set("gangSettings", gang.gangSettings);
        update(gang.name, ops);
    }

    private static Query<Gang> createQuery(String name) {
        return GangDAO.getInstance().createQuery().field("name").equal(name.toLowerCase());
    }

    private static UpdateResults<Gang> update(String name, UpdateOperations<Gang> ops) {
        return GangDAO.getInstance().update(createQuery(name), ops);
    }

}

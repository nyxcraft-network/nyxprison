package net.nyxcraft.dev.prison.database.entities;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

@Entity(value = "network_prison_gangs", noClassnameStored = true)
public class Gang {

    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String name;
    @Indexed
    public String casedName;
    public List<String> members = new ArrayList<>();
    public String leader;
    @Embedded
    public GangSettings gangSettings = new GangSettings();

    public Gang(String leader, String name) {
        this.name = name.toLowerCase();
        this.casedName = name;
        this.leader = leader;
    }
    
    public Gang() {}

    @PostLoad
    public void postLoad() {
        if (gangSettings == null) {
            gangSettings = new GangSettings();
        }
    }
}

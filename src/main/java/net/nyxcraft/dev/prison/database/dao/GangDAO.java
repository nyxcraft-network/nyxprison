package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.Gang;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class GangDAO extends BasicDAO<Gang, ObjectId> {

    private static GangDAO instance;

    public GangDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static GangDAO getInstance() {
        return instance;
    }

}

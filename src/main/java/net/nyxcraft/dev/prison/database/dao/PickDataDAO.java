package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.PickData;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class PickDataDAO extends BasicDAO<PickData, ObjectId> {

    private static PickDataDAO instance;

    public PickDataDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static PickDataDAO getInstance() {
        return instance;
    }

}

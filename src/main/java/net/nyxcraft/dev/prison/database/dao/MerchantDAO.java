package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.Merchant;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class MerchantDAO extends BasicDAO<Merchant, ObjectId> {

    private static MerchantDAO instance;

    public MerchantDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static MerchantDAO getInstance() {
        return instance;
    }

}

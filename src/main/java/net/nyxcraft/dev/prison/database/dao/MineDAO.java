package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.Mine;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class MineDAO extends BasicDAO<Mine, ObjectId> {

    private static MineDAO instance;

    public MineDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static MineDAO getInstance() {
        return instance;
    }

}

package net.nyxcraft.dev.prison.database.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity(value = "network_prisoner_pickaxe", noClassnameStored = true)
public class PickData {

    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    public int exp = 0;
    public int level = 1;
    public int blocksMined = 0;
    public int upgrades = 0;
    public int quantumUpgrades = 0;
    public boolean silkEnabled = false;
    public boolean visionEnabled = false;
    public boolean speedEnabled = false;
    public Map<Integer, Integer> enchantments = new HashMap<Integer, Integer>();

    public PickData(UUID uuid) {
        this.uuid = uuid.toString();
    }

    public PickData() {}

    public int availableUpgrades() {
        int assigned = 0;
        for (Integer perk : enchantments.keySet()) {
            int level = enchantments.get(perk).intValue();
            switch (perk) {
                case 32:
                case 35:
                    assigned += 1 * level;
                    continue;
                case 3:
                    assigned += 5 * level;
                    continue;
                case 1:
                case 16:
                case 33:
                    assigned += 8;
                    continue;
                default:
                    continue;
            }
        }
        return upgrades + quantumUpgrades - assigned;
    }
}

package net.nyxcraft.dev.prison.database.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

@Entity(value = "network_prison_merchants", noClassnameStored = true)
public class Merchant {

    @Id
    public ObjectId objectId;

    @Indexed(unique = true)
    public int id;
    @Indexed
    public String location;
    
    public Merchant(int id) {
        this.id = id;
    }
    
    public Merchant() {}
    
}

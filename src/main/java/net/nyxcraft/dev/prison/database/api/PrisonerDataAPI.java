package net.nyxcraft.dev.prison.database.api;

import java.util.UUID;

import net.nyxcraft.dev.prison.database.dao.PrisonerDataDAO;
import net.nyxcraft.dev.prison.database.entities.Cell;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;
import net.nyxcraft.dev.prison.database.entities.PrisonerSettings;
import net.nyxcraft.dev.prison.player.PrisonRank;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;

import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

public class PrisonerDataAPI {

    public static void saveAll(PrisonerSessionData data) {
        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.set("prestige", data.prestige);
        ops.set("currentRank", data.rank);
        ops.set("balance", data.balance);
        ops.set("pickData", data.pickData);
        update(data.uuid, ops);
    }

    public static void addCell(PrisonerSessionData data, Cell cell) {
        if (cell.id == null) {
            CellAPI.create(cell);
        }

        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.add("cells", cell);
        update(data.uuid, ops);
    }

    public static void prestige(UUID uuid) {
        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.inc("prestige");
        update(uuid, ops);
    }

    public static void setGang(PrisonerSessionData data) {
        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.set("gang", data.gang);
        update(data.uuid, ops);
    }

    public static void setBalance(PrisonerSessionData data) {
        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.set("balance", data.balance);
        update(data.uuid, ops);
    }

    public static void removeGang(UUID uuid) {
        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.unset("gang");
        update(uuid, ops);
    }

    public static void rankUp(PrisonerSessionData data) {
        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.set("currentRank", data.rank.ordinal());
        ops.set("balance", data.balance);
        update(data.uuid, ops);
    }

    public static void prestige(PrisonerSessionData data) {
        UpdateOperations<PrisonerData> ops = PrisonerDataDAO.getInstance().createUpdateOperations();
        ops.set("currentRank", PrisonRank.A.ordinal());
        ops.inc("prestige");
        ops.set("balance", 0);
        PickDataAPI.savePickData(data);
        update(data.uuid, ops);
    }

    public static void updatePrisonerSettings(UUID uuid, PrisonerSettings settings) {
        UpdateOperations<PrisonerData> operations = PrisonerDataDAO.getInstance().createUpdateOperations();
        operations.set("prisonerSettings", settings);
        Query<PrisonerData> query = PrisonerDataDAO.getInstance().createQuery().field("uuid").equal(uuid.toString());
        PrisonerDataDAO.getInstance().update(query, operations);
    }

    public static void setGuard(UUID uuid, boolean guard, boolean guardEnabled) {
        UpdateOperations<PrisonerData> operations = PrisonerDataDAO.getInstance().createUpdateOperations();
        operations.set("guard", guard);
        operations.set("guardEnabled", guardEnabled);
        Query<PrisonerData> query = PrisonerDataDAO.getInstance().createQuery().field("uuid").equal(uuid.toString());
        PrisonerDataDAO.getInstance().update(query, operations);
    }

    private static Query<PrisonerData> createQueryFromUuid(UUID uuid) {
        return PrisonerDataDAO.getInstance().createQuery().field("uuid").equal(uuid.toString());
    }

    private static UpdateResults<PrisonerData> update(UUID uuid, UpdateOperations<PrisonerData> ops) {
        return PrisonerDataDAO.getInstance().update(createQueryFromUuid(uuid), ops);
    }

}

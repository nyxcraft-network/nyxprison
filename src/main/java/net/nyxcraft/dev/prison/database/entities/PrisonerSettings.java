package net.nyxcraft.dev.prison.database.entities;

public class PrisonerSettings {

    private boolean pvpEnabled = true;
    private long pvpLastToggled = 0;
    private transient boolean dirty = false;

    public boolean isPvpEnabled() {
        return pvpEnabled;
    }

    public void setPvpEnabled(boolean pvpEnabled) {
        this.pvpEnabled = pvpEnabled;
        this.dirty = true;
    }

    public long getPvpLastToggled() {
        return pvpLastToggled;
    }

    public void setPvpLastToggled(long pvpLastToggled) {
        this.pvpLastToggled = pvpLastToggled;
        this.dirty = true;
    }

    public boolean isDirty() {
        return dirty;
    }
}

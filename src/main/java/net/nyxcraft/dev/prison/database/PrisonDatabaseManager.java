package net.nyxcraft.dev.prison.database;

import java.util.UUID;

import net.nyxcraft.dev.ndb.NyxDB;
import net.nyxcraft.dev.ndb.mongodb.ResourceManager;
import net.nyxcraft.dev.prison.database.dao.CellDAO;
import net.nyxcraft.dev.prison.database.dao.GangDAO;
import net.nyxcraft.dev.prison.database.dao.GuardDAO;
import net.nyxcraft.dev.prison.database.dao.MerchantDAO;
import net.nyxcraft.dev.prison.database.dao.MineDAO;
import net.nyxcraft.dev.prison.database.dao.PickDataDAO;
import net.nyxcraft.dev.prison.database.dao.PrisonDataDAO;
import net.nyxcraft.dev.prison.database.dao.PrisonerDataDAO;
import net.nyxcraft.dev.prison.database.entities.Gang;
import net.nyxcraft.dev.prison.database.entities.Guard;
import net.nyxcraft.dev.prison.database.entities.Merchant;
import net.nyxcraft.dev.prison.database.entities.Mine;
import net.nyxcraft.dev.prison.database.entities.PrisonData;
import net.nyxcraft.dev.prison.database.entities.PrisonerData;

import org.bukkit.entity.Player;
import org.mongodb.morphia.Datastore;

public class PrisonDatabaseManager {

    private ResourceManager resourceManager;
    private Datastore datastore;

    public PrisonDatabaseManager() {
        resourceManager = NyxDB.getResourceManager();
        datastore = resourceManager.getDatastore("net.nyxcraft.dev.prison.database.entities");
        init();
    }

    private void init() {
        datastore.ensureCaps();
        datastore.ensureIndexes();

        registerDAOs();
    }

    private void registerDAOs() {
        new PrisonerDataDAO(datastore);
        new GangDAO(datastore);
        new MineDAO(datastore);
        new PrisonDataDAO(datastore);
        new GuardDAO(datastore);
        new MerchantDAO(datastore);
        new PickDataDAO(datastore);
        new CellDAO(datastore);
    }

    public static PrisonerData initPrisonerData(Player player) {
        PrisonerData prisonerData = getPrisonerData(player.getUniqueId());
        if (prisonerData == null) {
            prisonerData = new PrisonerData(player);
            PrisonerDataDAO.getInstance().save(prisonerData);
        }
        return prisonerData;
    }

    public static PrisonerData initPrisonerData(UUID uuid, String name) {
        PrisonerData prisonerData = getPrisonerData(uuid);
        if (prisonerData == null) {
            prisonerData = createPrisonerData(uuid, name);
        }
        return prisonerData;
    }

    public static PrisonerData createPrisonerData(UUID uuid, String name) {
        PrisonerData prisonerData = new PrisonerData(uuid, name);
        PrisonerDataDAO.getInstance().save(prisonerData);
        return prisonerData;
    }

    public static PrisonData getPrisonData() {
        PrisonData prisonData = PrisonDataDAO.getInstance().find().get();
        if (prisonData == null) {
            prisonData = new PrisonData();
        }
        return prisonData;
    }

    public static PrisonerData getPrisonerData(UUID uuid) {
        return PrisonerDataDAO.getInstance().findOne("uuid", uuid.toString());
    }

    public static PrisonerData getPrisonerData(String name) {
        for (PrisonerData user : PrisonerDataDAO.getInstance().createQuery().field("name").containsIgnoreCase(name).fetch()) {
            if (user.name.equalsIgnoreCase(name)) {
                return user;
            }
        }

        return null;
    }

    public static Gang getGang(String name) {
        if (name == null) {
            return null;
        }

        return GangDAO.getInstance().findOne("name", name.toLowerCase());
    }

    public static Mine getMine(String name) {
        if (name == null) {
            return null;
        }

        return MineDAO.getInstance().findOne("name", name.toLowerCase());
    }

    public static Guard getGuard(int id) {
        return GuardDAO.getInstance().findOne("id", id);
    }

    public static Merchant getMerchant(int id) {
        return MerchantDAO.getInstance().findOne("id", id);
    }

}

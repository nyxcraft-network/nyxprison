package net.nyxcraft.dev.prison.database.api;

import net.nyxcraft.dev.prison.database.dao.CellDAO;
import net.nyxcraft.dev.prison.database.entities.Cell;

import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

import java.util.UUID;

public class CellAPI {

    public static void create(Cell cell) {
        CellDAO.getInstance().save(cell);
    }

    private static Query<Cell> createQueryFromUuid(UUID uuid) {
        return CellDAO.getInstance().createQuery().field("uuid").equal(uuid.toString());
    }

    @SuppressWarnings({ "unused" })
    private static UpdateResults<Cell> update(UUID uuid, UpdateOperations<Cell> ops) {
        return CellDAO.getInstance().update(createQueryFromUuid(uuid), ops);
    }

}

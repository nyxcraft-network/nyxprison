package net.nyxcraft.dev.prison.database.api;

import net.nyxcraft.dev.prison.database.dao.PrisonDataDAO;
import net.nyxcraft.dev.prison.database.entities.PrisonData;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

public class PrisonDataAPI {

    public static void incrementCurrentCellId() {
        UpdateOperations<PrisonData> ops = PrisonDataDAO.getInstance().createUpdateOperations();
        ops.inc("currentCellId");
        update(ops);
    }

    private static Query<PrisonData> createQuery() {
        return PrisonDataDAO.getInstance().createQuery();
    }

    private static UpdateResults<PrisonData> update(UpdateOperations<PrisonData> ops) {
        return PrisonDataDAO.getInstance().update(createQuery(), ops);
    }

}

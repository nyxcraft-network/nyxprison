package net.nyxcraft.dev.prison.database.entities;

import java.util.UUID;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;

import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

@Entity(value = "network_prison_cell", noClassnameStored = true)
public class Cell {

    @Id
    public ObjectId id;
    @Indexed
    public String uuid;
    public int chunkX;
    public int chunkZ;

    public Cell(UUID uuid, int chunkX, int chunkZ) {
        this.uuid = uuid.toString();
        this.chunkX = chunkX;
        this.chunkZ = chunkZ;
    }

    public Cell() {}

    public void teleportToCell(Player player) {
        player.teleport(Bukkit.getWorld("cellworld").getBlockAt(chunkX * 16 + 8, 55, chunkZ * 16 - 2).getLocation());//.getChunkAt(chunkX, chunkZ).getBlock(8, 55, 4).getLocation());
        MessageFormatter.sendSuccessMessage(player, "Teleported to the cell.");
    }

}

package net.nyxcraft.dev.prison.database.entities;

import java.util.ArrayList;
import java.util.List;

import net.nyxcraft.dev.prison.command.MineCommands;
import net.nyxcraft.dev.prison.command.MineCommands.MineBlockData;
import net.nyxcraft.dev.prison.player.PrisonRank;

import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.patterns.BlockChance;
import com.sk89q.worldedit.patterns.RandomFillPattern;

@SuppressWarnings("deprecation")
@Entity(value = "network_prison_mines", noClassnameStored = true)
public class Mine {

    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String name;
    @Indexed
    public String casedName;
    public int minX;
    public int minY;
    public int minZ;
    public int maxX;
    public int maxY;
    public int maxZ;
    public String world;
    public String ores;
    public String requiredPrisonRank;
    public String warpLocation;

    public Mine(String name) {
        this.name = name;
    }

    public Mine() {
    }

    public PrisonRank getRequiredRank() {
        PrisonRank rank;
        try {
            rank = PrisonRank.valueOf(requiredPrisonRank);
        } catch (NullPointerException ex) {
            return PrisonRank.A;
        }
        if (rank == null) {
            return PrisonRank.A;
        } else {
            return rank;
        }
    }

    public boolean isIn(Location loc) {
        if (loc.getBlockX() < minX)
            return false;
        if (loc.getBlockX() > maxX)
            return false;
        if (loc.getBlockY() < minY)
            return false;
        if (loc.getBlockY() > maxY)
            return false;
        if (loc.getBlockZ() < minZ)
            return false;
        if (loc.getBlockZ() > maxZ)
            return false;
        return true;
    }

    public void reset() {
        EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(BukkitUtil.getLocalWorld(Bukkit.getWorld(world)), -1);
        List<BlockChance> blocks = new ArrayList<BlockChance>();
        List<MineBlockData> blockDatas = MineCommands.stringToMineBlockData(ores);
        double totalPercent = 0;
        for (MineBlockData blockData : blockDatas) {
            totalPercent += blockData.percent;
            BaseBlock bb = new BaseBlock(blockData.id, blockData.data);
            blocks.add(new BlockChance(bb, blockData.percent));
        }
        blocks.add(new BlockChance(new BaseBlock(0), 100.0 - totalPercent));
        RandomFillPattern pattern = new RandomFillPattern(blocks);
        World world = Bukkit.getWorld(this.world);
        CuboidSelection selection = new CuboidSelection(world, new Location(world, minX, minY, minZ), new Location(world, maxX, maxY, maxZ));
        try {
            editSession.setBlocks(selection.getRegionSelector().getRegion(), pattern);
        } catch (MaxChangedBlocksException | IncompleteRegionException e) {
            e.printStackTrace();
        }
    }
}

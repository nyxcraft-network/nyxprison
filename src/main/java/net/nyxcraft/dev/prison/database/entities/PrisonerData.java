package net.nyxcraft.dev.prison.database.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.nyxcraft.dev.prison.database.dao.PickDataDAO;
import org.bson.types.ObjectId;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.*;

@Entity(value = "network_prisoner_data", noClassnameStored = true)
public class PrisonerData {

    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    @Indexed
    public String name;
    public int prestige = 0;
    public int currentRank = 0;
    public double balance = 0.0;
    public String gang;
    @Reference
    public List<Cell> cells = new ArrayList<Cell>();
    @Reference
    public PickData pickData;
    public PrisonerSettings prisonerSettings =  new PrisonerSettings();
    public boolean guard = false;
    public boolean guardEnabled = false;

    public PrisonerData(Player player) {
        this.uuid = player.getUniqueId().toString();
        this.name = player.getName();
        pickData = new PickData(player.getUniqueId());
        PickDataDAO.getInstance().save(pickData);
    }

    public PrisonerData(UUID uuid, String name) {
        this.uuid = uuid.toString();
        this.name = name;
        pickData = new PickData(uuid);
        PickDataDAO.getInstance().save(pickData);
    }

    public PrisonerData() {
    }

    @PostLoad
    public void postLoad() {
        if (prisonerSettings == null) {
            prisonerSettings = new PrisonerSettings();
        }
    }
}

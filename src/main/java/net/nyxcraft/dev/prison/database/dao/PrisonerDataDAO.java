package net.nyxcraft.dev.prison.database.dao;

import net.nyxcraft.dev.prison.database.entities.PrisonerData;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class PrisonerDataDAO extends BasicDAO<PrisonerData, ObjectId> {

    private static PrisonerDataDAO instance;

    public PrisonerDataDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static PrisonerDataDAO getInstance() {
        return instance;
    }

}

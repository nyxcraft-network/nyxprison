package net.nyxcraft.dev.prison.database.api;

import net.nyxcraft.dev.prison.database.dao.PickDataDAO;
import net.nyxcraft.dev.prison.database.entities.PickData;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

import java.util.UUID;

public class PickDataAPI {

    public static void savePickData(PrisonerSessionData data) {
        PickDataDAO.getInstance().save(data.pickData);
    }

    public static void savePickData(PickData data) {
        PickDataDAO.getInstance().save(data);
    }

    public static void savePickUpgrades(UUID uuid, PickData data) {
        UpdateOperations<PickData> ops = PickDataDAO.getInstance().createUpdateOperations();
        ops.set("upgrades", data.upgrades);
        ops.set("quantumUpgrades", data.quantumUpgrades);
        update(uuid, ops);
    }

    public static void savePickEnchants(PrisonerSessionData data) {
        UpdateOperations<PickData> ops = PickDataDAO.getInstance().createUpdateOperations();
        ops.set("enchantments", data.pickData.enchantments);
        update(data.uuid, ops);
    }

    public static void savePickEnchants(UUID uuid, PickData data) {
        UpdateOperations<PickData> ops = PickDataDAO.getInstance().createUpdateOperations();
        ops.set("enchantments", data.enchantments);
        update(uuid, ops);
    }

    private static Query<PickData> createQuery(UUID uuid) {
        return PickDataDAO.getInstance().createQuery().field("uuid").equal(uuid.toString());
    }

    private static UpdateResults<PickData> update(UUID uuid, UpdateOperations<PickData> ops) {
        return PickDataDAO.getInstance().update(createQuery(uuid), ops);
    }

}

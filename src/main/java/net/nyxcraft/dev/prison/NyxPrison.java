package net.nyxcraft.dev.prison;

import java.util.*;

import net.minecraft.server.v1_8_R1.EntityVillager;
import net.minecraft.server.v1_8_R1.EntityWitch;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.ActionAnnouncer;
import net.nyxcraft.dev.prison.command.*;
import net.nyxcraft.dev.prison.database.PrisonDatabaseManager;
import net.nyxcraft.dev.prison.database.dao.GuardDAO;
import net.nyxcraft.dev.prison.database.dao.MerchantDAO;
import net.nyxcraft.dev.prison.database.dao.MineDAO;
import net.nyxcraft.dev.prison.database.entities.Guard;
import net.nyxcraft.dev.prison.database.entities.Merchant;
import net.nyxcraft.dev.prison.database.entities.Mine;
import net.nyxcraft.dev.prison.database.entities.PrisonData;
import net.nyxcraft.dev.prison.economy.GuardEntity;
import net.nyxcraft.dev.prison.economy.MerchantEntity;
import net.nyxcraft.dev.prison.listener.*;
import net.nyxcraft.dev.prison.menu.WarpMenu.WarpData;
import net.nyxcraft.dev.prison.player.PrisonerSessionData;
import net.nyxcraft.dev.prison.world.CellGenerator;
import net.nyxcraft.dev.prison.world.MineSessionData;
import net.nyxcraft.dev.prison.world.SchematicManager;

import net.nyxcraft.dev.prison.world.VoidGenerator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

public class NyxPrison extends JavaPlugin {

    private static NyxPrison instance = null;

    private Map<UUID, PrisonerSessionData> prisoners = new HashMap<>();
    public Map<String, WarpData> warps = new HashMap<>();
    @SuppressWarnings("unused")
    private PrisonDatabaseManager databaseManager;
    private SchematicManager schematicManager;
    private List<MineSessionData> mines = new ArrayList<>();
    private PrisonData data;
    private ActionAnnouncer actionAnnouncer;

    public void onEnable() {
        instance = this;
        init();
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> actionAnnouncer = new ActionAnnouncer(NyxCore.getNetworkSettings().announcements.get("prison") == null ? new ArrayList<>() : NyxCore.getNetworkSettings().announcements.get("prison"), 5), 20 * 5);
    }

    public void onDisable() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            ConnectionListener.saveUser(player);
        }

        for (World world : Bukkit.getWorlds()) {
            for (org.bukkit.entity.Entity entity : world.getEntities()) {
                if (entity.getType() == EntityType.VILLAGER) {
                    EntityVillager e2 = (EntityVillager) ((CraftEntity) entity).getHandle();
                    if (e2 instanceof GuardEntity) {
                        entity.remove();
                    }
                }
                if (entity.getType() == EntityType.WITCH) {
                    EntityWitch e2 = (EntityWitch) ((CraftEntity) entity).getHandle();
                    if (e2 instanceof MerchantEntity) {
                        entity.remove();
                    }
                }
            }
        }
    }

    private void init() {
        databaseManager = new PrisonDatabaseManager();
        data = PrisonDatabaseManager.getPrisonData();
        initSchematicManager();
        loadMineWorld();
        loadCellWorld();
        cleanWorlds();
        registerListeners();
        registerCommands();
        loadMines();
        loadGuards();
        loadMerchants();
        loadWarps();
    }

    public void cleanWorlds() {
        for (World world : Bukkit.getWorlds()) {
            world.setStorm(false);
            for (org.bukkit.entity.Entity e : world.getEntities()) {
                if (e.getType() != EntityType.PLAYER) {
                    e.remove();
                }
            }
            if (world.getEnvironment() == World.Environment.NORMAL) {
                world.setThundering(true);
                world.setStorm(false);
            }
        }

    }

    public void loadMineWorld() {
        World mineWorld = Bukkit.getWorld("mines");
        if (mineWorld == null) {
            mineWorld = Bukkit.createWorld(new WorldCreator("mines").generator(new VoidGenerator()));
            mineWorld.setGameRuleValue("doDaylightCycle", "false");
            mineWorld.setGameRuleValue("doFireTick", "false");
            mineWorld.setGameRuleValue("doMobSpawning", "true");
            mineWorld.setGameRuleValue("mobGriefing", "false");
            mineWorld.setTime(18000);
        }
    }

    public void loadCellWorld() {
        World cellworld = Bukkit.getWorld("cellworld");
        if (cellworld == null) {
            cellworld = Bukkit.createWorld(new WorldCreator("cellworld").environment(World.Environment.NORMAL).generator(new CellGenerator()));
            cellworld.setGameRuleValue("doDaylightCycle", "false");
            cellworld.setGameRuleValue("doFireTick", "false");
            cellworld.setGameRuleValue("doMobSpawning", "false");
            cellworld.setGameRuleValue("mobGriefing", "false");
            cellworld.setTime(18000);
            cellworld.setSpawnLocation(0, 55, 0);
        }
    }

    public void loadMines() {
        for (Mine mine : MineDAO.getInstance().createQuery()) {
            int broken = 0;
            int total = 0;
            World world = Bukkit.getWorld(mine.world);
            if (world != null) {
                for (int x = mine.minX; x <= mine.maxX; x++) {
                    for (int y = mine.minY; y <= mine.maxY; y++) {
                        for (int z = mine.minZ; z <= mine.maxZ; z++) {
                            if (world.getBlockAt(x, y, z).getType() == Material.AIR) {
                                broken++;
                            }
                            total++;
                        }
                    }
                }
            }
            MineSessionData session = new MineSessionData(mine, broken - 1, total);
            session.addBlock();
            mines.add(session);
        }
    }

    public void loadGuards() {
        for (Guard guard : GuardDAO.getInstance().createQuery()) {
            GuardEntity.loadGuard(PrisonCommands.stringToLocation(guard.location), guard.id);
        }
    }

    public void loadMerchants() {
        for (Merchant merchant : MerchantDAO.getInstance().createQuery()) {
            MerchantEntity.loadMerchant(PrisonCommands.stringToLocation(merchant.location), merchant.id);
        }
    }

    public void loadWarps() {
        Map<String, String> warps = PrisonDatabaseManager.getPrisonData().warps;
        Map<String, Material> warpMaterials = PrisonDatabaseManager.getPrisonData().warpItems;
        for (String warp : warps.keySet()) {
            this.warps.put(warp, new WarpData(PrisonCommands.stringToLocation(warps.get(warp)), warpMaterials.get(warp)));
        }
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new ConnectionListener(), this);
        Bukkit.getPluginManager().registerEvents(new BuildListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryListener(), this);
        Bukkit.getPluginManager().registerEvents(new InteractListener(), this);
        Bukkit.getPluginManager().registerEvents(new LifeDeathListener(), this);
        Bukkit.getPluginManager().registerEvents(new WeatherListener(), this);
        Bukkit.getPluginManager().registerEvents(new CellListener(), this);
        Bukkit.getPluginManager().registerEvents(new PVPListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChunkListener(), this);
        Bukkit.getPluginManager().registerEvents(new CommandListener(), this);
    }

    private void registerCommands() {
        new GuardCommands();
        new GangCommands();
        new RanksCommands();
        new MineCommands();
        new EconomyCommands();
        new PrisonCommands();
        new CellCommands();
        new PickCommands();
        new DonorCommands();
    }

    public static NyxPrison getInstance() {
        return instance;
    }

    public void initSchematicManager() {
        this.schematicManager = new SchematicManager();
    }

    public ChunkGenerator getDefaultWorldGenerator(String worldName, String GenId) {
        return new VoidGenerator();
    }

    public MineSessionData getMine(String name) {
        for (MineSessionData session : mines) {
            if (session.getMine().name.equalsIgnoreCase(name)) {
                return session;
            }
        }
        return null;
    }

    public void addPrisoner(PrisonerSessionData prisonerSessionData) {
        prisoners.put(prisonerSessionData.uuid, prisonerSessionData);
    }

    public PrisonerSessionData getPrisonerSessionData(UUID uuid) {
        return prisoners.get(uuid);
    }

    public SchematicManager getSchematicManager() {
        return schematicManager;
    }

    public PrisonData getData() {
        return data;
    }

    public List<MineSessionData> getMines() {
        return mines;
    }

    public ActionAnnouncer getActionAnnouncer() {
        return actionAnnouncer;
    }
}
